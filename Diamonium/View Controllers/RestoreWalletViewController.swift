//
//  RestoreWalletViewController.swift
//  Diamonium
//
//

import UIKit
import SkyFloatingLabelTextField
import FlagPhoneNumber

class RestoreWalletViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldMobile: FPNTextField!
    @IBOutlet weak var labelMobilePrefix: UILabel!
    
    var countryCode: String?
  
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldMobile.font = UIFont.systemFont(ofSize: 14)
        textFieldMobile.delegate = self
        labelMobilePrefix.text = Locale.current.currencyCode
        countryCode = textFieldMobile.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "")
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            textFieldMobile.setFlag(for: FPNCountryCode(rawValue: countryCode)!)
        }
    }
    
    //MARK: Action
    @IBAction func showPassword(_ sender: UIButton) {
        if textFieldPassword.isSecureTextEntry {
            textFieldPassword.isSecureTextEntry = false
        } else {
            textFieldPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func restore(_ sender: UIButton) {
        if (textFieldMobile.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Username", action1Name: "Ok", action2Name: nil)
        } else if (textFieldPassword.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            let mobilenumber = ((labelMobilePrefix.text)?.lowercased())! + "-" + (countryCode ?? "") + textFieldMobile.text!.replacingOccurrences(of: " ", with: "")
            let param = [K_USERNAME: mobilenumber,
                         K_PRIVATE_KEY: textFieldPassword.text!,
                         ] as [String : Any]
           
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_VERIFY_KEY, method: .post, parameter: param, objectClass: ResetResponse.self, requestCode: U_VERIFY_KEY, userToken: nil) { response in
                let resetController = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
                resetController.userName = mobilenumber
                resetController.privateKey = self.textFieldPassword.text!
                self.navigationController?.pushViewController(resetController, animated: true)
                ActivityIndicator.hide()
            }
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        let loginController = storyboard?.instantiateViewController(withIdentifier: "RestoreWalletViewController") as! RestoreWalletViewController
        navigationController?.pushViewController(loginController, animated: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension RestoreWalletViewController: FPNTextFieldDelegate {
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        return
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        countryCode = dialCode.replacingOccurrences(of: "+", with: "")
        let currency = (Locale.currency[code])!
        if let currencyCode = (currency as? (String, String))?.0 {
            labelMobilePrefix.text = currencyCode
        }
        
    }
}
