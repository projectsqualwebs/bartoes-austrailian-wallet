//
//  TransactionViewController.swift
//  Diamonium
//
//

import UIKit
import RealmSwift


class TransactionViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableViewTransaction: UITableView!
    @IBOutlet weak var labelNoTransaction: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var labelRecievedAmount: UILabel!
    @IBOutlet weak var labelForTo: UILabel!
    @IBOutlet weak var labelForFrom: UILabel!
    @IBOutlet weak var labelForDate: UILabel!
    @IBOutlet weak var labelForBlockId: UILabel!
    @IBOutlet weak var labelForAuthorizer: UILabel!
    @IBOutlet weak var labelForStatus: UILabel!
    @IBOutlet weak var transactionimage: UIImageView!
    @IBOutlet weak var labelForMemoText: UILabel!
    @IBOutlet weak var viewTransactionFees: UIStackView!
    @IBOutlet weak var buttonForLatestUpdate: UIButton!
    
    @IBOutlet weak var viewForMemoText: UIStackView!
    
    var transactionList = [Transaction]()
    var page: Int = 0
    var transactionCount: Int = 0
    var totalCount: Int = 0
    var dataArray : [String] = []
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTransactionFees.isHidden = true
        viewForMemoText.isHidden = true
        popupView.isHidden = true
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
       tableViewTransaction.addSubview(refreshControl)
        if (UserDefaults.standard.value(forKey: "transUpdate") as? String == "Show loader") {
          buttonForLatestUpdate.isHidden = true
          UserDefaults.standard.set("Hide loader", forKey: "transUpdate")
        } else {
          buttonForLatestUpdate.isHidden = true
        }
        self.transactionList = [Transaction]()
        getTransactions()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        self.updateTransactionAction(self)
    }
    
    func getTransactions() {
        let transactions = DBManager.sharedInstance.getTransactionFromDB().sorted(byKeyPath: "dateTime", ascending: false)
        for trx in transactions {
            if trx.trxid == "0" {
                buttonForLatestUpdate.isHidden = false
            }
            let id = trx.trxid
            let name = trx.name
            let amount = trx.amount
            let type = trx.type
            let dateTime = trx.dateTime
            let blockId = trx.blockId
            let memoText = trx.memoText ?? ""
            let to_name = trx.toName ?? ""
            let authorizer = trx.authorizer ?? ""
            let asset = trx.asset ?? ""
            self.labelForDate.text = dateTime.description.replacingOccurrences(of: "T", with: " ")
            if(trx.type != "3"){
                transactionList.append(Transaction(id: id, name: name, dateTime: dateTime, amount: "\(amount!)", type: Int(type!)!, blockId: Int(blockId ?? "0")!,memoText:memoText,to_name:to_name,authorizer: authorizer, asset: asset ))
            }
        }
        
        let transData = transactionList.unique{$0.trxid ?? "0"}
        self.transactionList = []
        self.transactionList = transData
        
        if totalCount < transactions.count && totalCount != 0  {
            buttonForLatestUpdate.isHidden = true
        }
        
        if transactions.count > 0 {
            tableViewTransaction.isHidden = false
            labelNoTransaction.isHidden = true
        } else {
            tableViewTransaction.isHidden = true
            labelNoTransaction.isHidden = false
        }
       tableViewTransaction.reloadData()
    }
    
    func createCSV() {
        var csvString = "\("Transaction Id"),\("To"),\("From"),\("Amount"),\("Type"),\("Date")\n\n"
        var userName = DBManager.sharedInstance.currentUser[0].accNumber
        for trx in transactionList {
            let dateTime = convertTimestampToDate(trx.datetime ?? 0, to: "yyyy-MM-dd hh:mm:ss")
            if trx.type == K_SENT {
                csvString = csvString.appending("\(trx.trxid ?? "0") ,\(trx.name ?? "0"),\(userName!), \(trx.amount ?? "0") ,\(trx.type ?? 0) ,\(dateTime)\n")
            }else {
                csvString = csvString.appending("\(trx.trxid ?? "0"),\(userName!) ,\(trx.name ?? "0"), \(trx.amount ?? "0") ,\(trx.type ?? 0) ,\(dateTime)\n")
            }
            
        }
        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .documentDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            let fileURL = path.appendingPathComponent("Transactions.csv")
            
            try csvString.write(to: fileURL, atomically: true, encoding: .utf8)
            showAlert(title: "Success", message: "CSV file created successfully", action1Name: "Ok", action2Name: nil)
        } catch {
            showAlert(title: "Error", message: "Error creating file", action1Name: "Ok", action2Name: nil)
        }
    }
    
    func showPopUp() {
      popupView.isHidden = false
    }
    
    //MARK: Actions
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func exportCSV(_ sender: UIButton) {
        createCSV()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        popupView.isHidden = true
    }
    
    @IBAction func updateTransactionAction(_ sender: Any) {
         NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTransactionTable(notification:)), name: Notification.Name("updateTransactionHistory"), object: nil)
        totalCount = transactionList.count
        ActivityIndicator.show(view: self.view)
        buttonForLatestUpdate.isHidden = true
        transactionList = []
        DBManager.sharedInstance.deleteTransactions()
         tableViewTransaction.reloadData()
        Router.getTransactionHistory(transId: "1.11.0")
   }

@objc  func reloadTransactionTable(notification: Notification) {
    ActivityIndicator.hide()
   // NotificationCenter.default.removeObserver(self, name: Notification.Name("updateTransactionHistory"), object: nil)
    getTransactions()
    tableViewTransaction.reloadData()
}
}

extension TransactionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionList.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
        let transaction = transactionList[indexPath.row]
        
        
        cell.labelTime.text = convertTimestampToDate(transaction.datetime ?? 0, to: "yyyy-MM-dd hh:mm:ss")
        cell.labelUsername.text = transaction.name?.uppercased()
        
        if transaction.type == K_SENT {
            cell.labelAmount.text = "-" + (transaction.amount?.description)!
            cell.profileImage.image = UIImage(named: "icons8-up-100")
            cell.labelAmount.textColor = UIColor(red: 178/255, green: 34/255, blue: 34/255, alpha: 1)
            
        } else {
            cell.profileImage.image = UIImage(named: "icons8-down-arrow-100")
            cell.labelAmount.text = transaction.amount?.description
            cell.labelAmount.textColor = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1)
        }
        
        if (cell.labelUsername.text == "AU-CASHBACK"){
            cell.bteLabel.text = "BTE-AUD"
        }else {
            cell.bteLabel.text = (transaction.asset ?? "BTE").uppercased()
        }
        
        return cell
    }
}

extension TransactionViewController: UITableViewDelegate {
    
    func getTransactionDetail(id:String?) {
        if buttonForLatestUpdate.isHidden == true {
          ActivityIndicator.show(view: self.view)
        }
        let finalUrl = U_BASE2+U_GET_SINGLE_TRANSACTION+(id ?? "0")
        SessionManager.shared.methodForApiCalling(url: finalUrl, method: .get, parameter: nil, objectClass: TransactionDetailData.self, requestCode: U_GET_SINGLE_TRANSACTION, userToken: nil) { (response) in
            
            self.labelForBlockId.text = response.data?.block_id?.description
            self.labelForAuthorizer.text = response.data?.authorizer
            self.labelForDate.text = response.data?.time?.description.replacingOccurrences(of: "T", with: " ")
            ActivityIndicator.hide()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transaction = transactionList[indexPath.row]
        self.showPopUp()
        print(transactionList[indexPath.row])
        self.getTransactionDetail(id: transaction.trxid)
        if (transaction.type == K_SENT) {
            self.labelForTo.text = transaction.name
            viewTransactionFees.isHidden = false
            viewForMemoText.isHidden = false
            self.labelForFrom.text = DBManager.sharedInstance.currentUser[0].accNumber
            self.labelForStatus.text = "Confirmed"
            self.transactionimage.image = UIImage(named:"icons8-up-100")
        } else {
            self.transactionimage.image = UIImage(named:"icons8-down-arrow-100")
            self.labelForFrom.text = transaction.name
            viewTransactionFees.isHidden = true
            viewForMemoText.isHidden = true
            self.labelForStatus.text = "Confirmed"
            self.labelForTo.text = DBManager.sharedInstance.currentUser[0].accNumber
        }
        self.labelRecievedAmount.text = "\((transaction.amount!.description + "0") ?? "0") BTE"
        
        self.labelForMemoText.text = transaction.memoText
//        self.labelForBlockId.text = response.data?.block_id?.description
//        self.labelForAuthorizer.text = response.data?.authorizer
//        self.labelForDate.text = response.data?.time?.description.replacingOccurrences(of: "T", with: " ")
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }

        return arrayOrdered
    }
}
