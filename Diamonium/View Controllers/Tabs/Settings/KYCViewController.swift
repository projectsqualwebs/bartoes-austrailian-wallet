//
//  KYCViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 16/10/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class KYCViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var kycImage: UIImageView!
    @IBOutlet weak var docNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var addressLine1: SkyFloatingLabelTextField!
    @IBOutlet weak var addressLine2: SkyFloatingLabelTextField!
    @IBOutlet weak var cityTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var stateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var postcodeTextfield: SkyFloatingLabelTextField!
    
    let picker = UIImagePickerController()
    var imageName: String = ""
    var imagePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self

    }
    
    //MARK: IBAction
    @IBAction func kycAction(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
               alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                   self.openCamera()
               }))
               
               alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                   self.openGallary()
               }))
               
               alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
               if let popoverController = alert.popoverPresentationController {
                   popoverController.sourceView = sender as! UIView
                   popoverController.sourceRect = (sender as AnyObject).bounds
               }
               self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func updateAction(_ sender: Any) {
        if(self.docNumber.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter document number", action1Name: "Ok", action2Name: nil)
        }else if(self.imagePath == ""){
            self.showAlert(title: "Error", message: "Upload KYC image", action1Name: "Ok", action2Name: nil)
        }else if(self.addressLine1.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter first address", action1Name: "Ok", action2Name: nil)
        }else if(self.addressLine2.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter second address", action1Name: "Ok", action2Name: nil)
        }else if(self.cityTextfield.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter city", action1Name: "Ok", action2Name: nil)
        }else if(self.stateTextField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter state", action1Name: "Ok", action2Name: nil)
        }else if(self.postcodeTextfield.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter postal code", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "address_line_1": self.addressLine1.text ?? "",
                "address_line_2": self.addressLine2.text ?? "",
                "city": self.cityTextfield.text ?? "",
                "state": self.stateTextField.text ?? "",
                "postcode": self.postcodeTextfield.text ?? "",
                "document_number": self.docNumber.text ?? "",
                "document_path": self.imagePath ?? ""
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_UPDATE_KYC, method: .post, parameter: param, objectClass: Response.self, requestCode: U_UPLOAD_WALLET_IMAGE, userToken: nil) { response in
                self.showAlert(title: response.message ?? "", message: nil, action1Name: "Ok", action2Name: nil)
                ActivityIndicator.hide()
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension KYCViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
        } else {
            self.imageName = "image.jpg"
        }
        
        if let cropImage = selectedImage as? UIImage {
                   let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
                   
                   let paramName = "image"
                   
                   // HTTP Request for upload Picture
                   ActivityIndicator.show(view: self.view)
                   SessionManager.shared.makeMultipartRequest(url: U_BASE2 + U_UPLOAD_WALLET_IMAGE, fileData: data!, param: paramName, fileName: imageName) { (path) in
                       self.kycImage.image = cropImage
                       self.imagePath = path
                   }
               }
         picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
