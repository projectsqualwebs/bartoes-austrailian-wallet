//
//  TransactionDetailViewController.swift
//  Diamonium
//
//

import UIKit

class TransactionDetailViewController: UIViewController {

    //MARK: IBOultet
    @IBOutlet weak var labeltype: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelReceiverName: UILabel!
    @IBOutlet weak var labelSenderName: UILabel!
    @IBOutlet weak var labelBlockId: UILabel!
    @IBOutlet weak var labelAuthorizer: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    var id: String?
    var name: String?
    var amount: String?
    var type: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if type == K_SENT {
            labeltype.text = "Sent"
            labelSenderName.text = DBManager.sharedInstance.currentUser[0].accNumber
            labelReceiverName.text = name
        } else {
            labeltype.text = "Received"
            labelSenderName.text = name
            labelReceiverName.text = DBManager.sharedInstance.currentUser[0].accNumber
        }
        labelAmount.text = "\(amount ?? "0") BTE"
        
        getTransactionDetail()
    }
    
    func getTransactionDetail() {
        ActivityIndicator.show(view: self.view)
        let finalUrl = U_BASE2+U_GET_SINGLE_TRANSACTION+(id ?? "0")
        SessionManager.shared.methodForApiCalling(url: finalUrl, method: .get, parameter: nil, objectClass: TransactionDetailData.self, requestCode: U_GET_SINGLE_TRANSACTION, userToken: nil) { (response) in
            
            self.labelBlockId.text = response.data?.block_id?.description
            self.labelAuthorizer.text = response.data?.authorizer
            self.labelDate.text = response.data?.time?.description.replacingOccurrences(of: "T", with: " ")
            ActivityIndicator.hide()
        }
    }
    
    //MARK: Actions
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
