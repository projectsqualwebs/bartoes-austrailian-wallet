//
//  SubSettingContentViewController.swift
//  Diamonium
//
//

import UIKit
import LocalAuthentication

class SubSettingContentViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var subSettingTableview: UITableView!
    
    
    var navBarTitle: String?
    var arrayContent: [SettingsHeading] = [SettingsHeading]()
    var currencyList: [Currency] = [Currency]()
    
    var previousIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       labelTitle.text = navBarTitle
        
        switch navBarTitle {
        case "Currency":
            for index in 0..<(currencyList.count) {
                if Singleton.shared.selectedCurrency.description == currencyList[index].description {
                    previousIndex = index
                    currencyList[index].isSelected = true
                } else {
                    currencyList[index].isSelected = false
                }
            }
            break
        default:
            print("Auto lock")
            for index in 0..<(arrayContent[0].content?.count)! {
                if Singleton.shared.autoLockTimer == arrayContent[0].content?[index].autoLock?.interval {
                    previousIndex = index
                    arrayContent[0].content?[index].isSelected = true
                } else {
                    arrayContent[0].content?[index].isSelected = false
                }
            }
        }
    }
    
    func setAutoLock(_ time: TimeInterval, index: Int) {
        Singleton.shared.autoLockTimer = time
        arrayContent[0].content?[previousIndex!].isSelected = false
        arrayContent[0].content?[index].isSelected = true
        subSettingTableview.reloadRows(at: [IndexPath(row: previousIndex!, section: 0), IndexPath(row: index, section: 0)], with: .none)
        previousIndex = index
        navigationController?.popViewController(animated: true)
    }
    
    @objc func lock() {
        let context = LAContext()
        context.localizedFallbackTitle = "Use Passcode"
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authError) {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonString) { (success, error) in
                if success {
                    print("Authenticated successfully")
                } else {
                    guard let err = error else {
                        return
                    }
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    //MARK: Actions
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension SubSettingContentViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if navBarTitle == "Currency" {
            return 1
        } else {
            return arrayContent.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if navBarTitle == "Currency" {
            return currencyList.count
        } else {
            return arrayContent[section].content!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell2", for: indexPath) as! SettingTableViewCell
        
        var selectedCell: Bool = false
        
        if navBarTitle == "Currency" {
            let content = currencyList[indexPath.row]
            selectedCell = content.isSelected
            cell.labelText.text = content.description
        } else {
            let content = arrayContent[indexPath.section].content![indexPath.row]
            selectedCell = content.isSelected
            if content.name == nil {
                cell.labelText.text = content.autoLock?.name
            } else {
                cell.labelText.text = content.name
            }
        }
        
        if selectedCell {
            cell.checkMark.isHidden = false
        } else {
            cell.checkMark.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (navBarTitle == "Currency") ? "ALL" : (arrayContent[section].title)
    }
}
extension SubSettingContentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell = tableView.cellForRow(at: indexPath) as? SettingTableViewCell
        
        if navBarTitle == "Currency" {
            currencyList[previousIndex!].isSelected = false
            currencyList[indexPath.row].isSelected = true
            
            DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: currencyList[indexPath.row], bteBalance: nil, currencyBalance: nil, beRewards: nil)
            
            tableView.reloadRows(at: [IndexPath(row: previousIndex!, section: 0), IndexPath(row: indexPath.row, section: 0)], with: .none)
            previousIndex = indexPath.row
            NotificationCenter.default.post(name: NSNotification.Name("updateCurrency"), object: nil)
            self.popToTabController()
        } else {
            var content = arrayContent[indexPath.section].content![indexPath.row]
            var value = ""
            if content.name == nil {
                value = (content.autoLock?.name)!
            } else {
                value = content.name!
            }
            
            switch value {
            case "Auto-Lock":
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "SubSettingContentViewController") as! SubSettingContentViewController
                VC.navBarTitle = content.name
                VC.arrayContent = [SettingsHeading(title: content.name, content: [Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), autoLock: AutoLock.immediate), Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), autoLock: AutoLock.oneMinute), Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), autoLock: AutoLock.fiveMinute), Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), autoLock: AutoLock.oneHour), Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), autoLock: AutoLock.fiveHours)])]
                self.navigationController?.pushViewController(VC, animated: true)
//                self.present(VC, animated: true, completion: nil)
                break
            case AutoLock.immediate.name:
                setAutoLock(AutoLock.immediate.interval, index: indexPath.row)
                break
            case AutoLock.oneMinute.name:
                setAutoLock(AutoLock.oneMinute.interval, index: indexPath.row)
                break
            case AutoLock.fiveMinute.name:
                setAutoLock(AutoLock.fiveMinute.interval, index: indexPath.row)
                break
            case AutoLock.oneHour.name:
                setAutoLock(AutoLock.oneHour.interval, index: indexPath.row)
                break
            case AutoLock.fiveHours.name:
                setAutoLock(AutoLock.fiveHours.interval, index: indexPath.row)
                break
            default:
                print("default")
            }
        }
    }
}
