//
//  RewardsViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 29/09/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit

class RewardsViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var rewardTable: UITableView!
    @IBOutlet weak var noTransactionLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    
    @IBOutlet weak var popupView: View!
    @IBOutlet weak var confirmationLabel: UILabel!
    
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var amountTypeLabel: UILabel!
    @IBOutlet weak var preferredPaymentLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var rewardData = [RewardResponse]()
    var topupRequestData = [CryptoOrderResponse]()
    var isTopupRequest = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRewardData()
        self.rewardTable.estimatedRowHeight = 80
        self.rewardTable.rowHeight = UITableView.automaticDimension
    }
    
    func getRewardData(){
        if (isTopupRequest) {
            self.headingLabel.text = "Topup Requests"
            let token = UserDefaults.standard.value(forKey:K_BTE_CRYPTO_TOKEN) as? String
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TOPUP_ORDER, method: .post, parameter: nil, objectClass: GetCryptoOrder.self, requestCode: U_GET_TOPUP_ORDER, userToken: token) { response in
                if((response.orders?.count ?? 0) != 0){
                    self.topupRequestData = response.orders!
                    self.rewardTable.reloadData()
                }
                ActivityIndicator.hide()
            }
        }else {
            if(DBManager.sharedInstance.currentUser.count > 0){
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARD + "\(DBManager.sharedInstance.currentUser[0].accNumber ?? "")", method: .get, parameter: nil, objectClass: [RewardResponse].self, requestCode: U_GET_REWARD, userToken: nil) { response in
                    self.rewardData = response
                    self.rewardTable.reloadData()
                    ActivityIndicator.hide()
                }
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
}

extension RewardsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !(self.isTopupRequest){
            self.noTransactionLabel.text = "No request found"
        if(self.rewardData.count == 0){
            self.noTransactionLabel.isHidden = false
        }else {
            self.noTransactionLabel.isHidden = true
        }
        return self.rewardData.count
        }else {
            self.noTransactionLabel.text = "No requests found"
            if(self.topupRequestData.count == 0){
                self.noTransactionLabel.isHidden = false
            }else {
                self.noTransactionLabel.isHidden = true
            }
            return self.topupRequestData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath) as! TransactionTableViewCell
        if(self.isTopupRequest){
        let val = self.topupRequestData[indexPath.row]
        cell.labelUsername.text = val.order_id
            cell.profileImage.image = UIImage(named: "receive")
            cell.labelAmount.text =  "BTE \(val.bte_coin ?? 0)"
            
        cell.labelTime.text = val.status
        }else {
            let val = self.rewardData[indexPath.row]
            cell.labelUsername.text = val.merchant
            cell.labelAmount.text =  (val.cashback_amount ?? "") + " BTE-AUD"
            cell.labelTime.text = val.created_at
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isTopupRequest){
            let val = self.topupRequestData[indexPath.row]
            if(val.status == "Pending"){
            self.confirmationLabel.text = "Please pay \(val.coin_value ?? "") \(val.coin_type ?? "") to above address"
            }else {
                self.confirmationLabel.text = val.status
            }
            self.addressLabel.text = val.coin_address
            self.orderIdLabel.text = val.order_id
            self.preferredPaymentLabel.text = val.coin_type
            self.amountTypeLabel.text = "BTE"
            self.amountLabel.text = "\(val.bte_coin ?? 0)"
            self.popupView.isHidden = false
        }
    }
    
}
