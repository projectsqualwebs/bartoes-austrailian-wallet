//
//  SettingsViewController.swift
//  Diamonium
//
//

import UIKit
import LocalAuthentication
import SkyFloatingLabelTextField
import PayPalCheckout

class SettingsViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var tableViewSettings: UITableView!
    @IBOutlet weak var privateKeyPopUp: UIView!
    @IBOutlet weak var contentView: View!
    @IBOutlet weak var labePrivateKey: UILabel!
    @IBOutlet weak var clipboard: View!
    @IBOutlet weak var serverView: UIView!
    @IBOutlet weak var firstServerView: View!
    @IBOutlet weak var secondServerView: View!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var popupView: View!
    @IBOutlet weak var privateKeyStackView: UIStackView!
    
    var arrayContent: [SettingsHeading] = [SettingsHeading]()
    var selectedServer = "BTE AUS"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = barBackgroundColor
        
        arrayContent.append(SettingsHeading(title: "", content: [Settings(icon: #imageLiteral(resourceName: "wallet"), name: "Accounts"),Settings(icon: #imageLiteral(resourceName: "logout(2)"), name: "Logout"),Settings(icon: #imageLiteral(resourceName: "reward-1"), name: "")]))
        
        var security = [Settings(icon: #imageLiteral(resourceName: "password"), name: "Passcode / Touch ID")]
        if Singleton.shared.enableAuthentication {
            security.append(Settings(icon: #imageLiteral(resourceName: "settings-1"), name: "Advanced Settings"))
        }
    
        arrayContent.append(SettingsHeading(title: "Security", content: security))
        if(DBManager.sharedInstance.currentUser[0].accType == "Business" || DBManager.sharedInstance.currentUser[0].accType == "Non-Profit"){
            let currency = [Settings(icon: #imageLiteral(resourceName: "currency"), name: "Currency"),Settings(icon: #imageLiteral(resourceName: "swapping"), name: "Swap"), Settings(icon: #imageLiteral(resourceName: "transaction"), name: "Transactions"),Settings(icon: #imageLiteral(resourceName: "reward"), name: "Cashback Rewards"), Settings(icon: #imageLiteral(resourceName: "receive"), name: "Topup Requests"),Settings(icon: #imageLiteral(resourceName: "ic_notification"), name: "Notifications"), Settings(icon: #imageLiteral(resourceName: "password"), name: "Private Key"),Settings(icon: #imageLiteral(resourceName: "kyc"), name: "KYC"), Settings(icon: #imageLiteral(resourceName: "MicrosoftTeams-image (2)"), name: "Delegate Server"),Settings(icon: #imageLiteral(resourceName: "support"), name: "Support")]
            arrayContent.append(SettingsHeading(title: "", content: currency))
        }else {
            let currency = [Settings(icon: #imageLiteral(resourceName: "currency"), name: "Currency"), Settings(icon: #imageLiteral(resourceName: "transaction"), name: "Transactions"),Settings(icon: #imageLiteral(resourceName: "reward"), name: "Cashback Rewards"), Settings(icon: #imageLiteral(resourceName: "receive"), name: "Topup Requests"),Settings(icon: #imageLiteral(resourceName: "ic_notification"), name: "Notifications"), Settings(icon: #imageLiteral(resourceName: "password"), name: "Private Key"),Settings(icon: #imageLiteral(resourceName: "kyc"), name: "KYC"), Settings(icon: #imageLiteral(resourceName: "MicrosoftTeams-image (2)"), name: "Delegate Server"),Settings(icon: #imageLiteral(resourceName: "support"), name: "Support")]
            arrayContent.append(SettingsHeading(title: "", content: currency))
        }
        //Settings(icon: #imageLiteral(resourceName: "transaction"), name: "Swap"),
        getSocialFromDB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(passCodeScreen == "PrivateKey"){
            self.showPopUp()
        }
    }
        
    func getSocialFromDB() {
        var community = [Settings]()
        let socialDB = DBManager.sharedInstance.getSocialFromDB()
        if socialDB.count > 0 {
            for social in socialDB {
                community.append(Settings(icon: UIImage(named: "\(social.imageName ?? "").png")!, name: social.name ?? "", url: social.url))
            }
            self.arrayContent.append(SettingsHeading(title: "Join community", content: community))
            self.tableViewSettings.reloadData()
        }
    }
    
    func showPopUp() {
        privateKeyPopUp.isHidden = false
        labePrivateKey.text = DBManager.sharedInstance.currentUser[0].key
        
        privateKeyPopUp.alpha = 0.3
        contentView.alpha = 0.5
        contentView.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        
        UIView.animate(withDuration: 0.5) {
            self.privateKeyPopUp.alpha = 1
            self.contentView.alpha = 1
            self.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    func hidePopUp() {
        UIView.animate(withDuration: 0.5, animations: {
            self.privateKeyPopUp.alpha = 0
            passCodeScreen = ""
            self.contentView.alpha = 0
            self.contentView.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        }) { (_) in
        }
    }
    
    //MARK: Actions
    @IBAction func closePopUp(_ sender: UIButton) {
        hidePopUp()
    }
    
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: labePrivateKey.text)
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func serverAction(_ sender: UIButton) {
        if(sender.tag == 1){
            self.firstServerView.backgroundColor = primaryColor
            self.secondServerView.backgroundColor = .lightGray
        }else if(sender.tag == 2){
            self.firstServerView.backgroundColor = .lightGray
            self.secondServerView.backgroundColor = primaryColor
        }else if(sender.tag == 3){
            self.selectedServer = self.firstServerView.backgroundColor == .lightGray ? "MAINNET":"BTE AUS"
            self.tableViewSettings.reloadData()
            self.serverView.isHidden = true
        }else if(sender.tag == 4){
            self.serverView.isHidden = true
        }
    }
    
    @IBAction func generateTokenAction(_ sender: Any) {
        if(self.emailField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Email Address", action1Name: "Ok", action2Name: nil)
        }else if(self.passwordField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GENERATE_TOKEN, method: .post, parameter: ["email":self.emailField.text, "password":self.passwordField.text], objectClass: RegisterUser.self, requestCode: U_GENERATE_TOKEN, userToken: nil) { response in
                ActivityIndicator.hide()
                self.popupView.isHidden = true
                if(response.token != "" || response.token != nil){
                    UserDefaults.standard.setValue(response.token ?? "", forKey: K_BTE_CRYPTO_TOKEN)
                    let VC = self.storyboard?.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
                    VC.isTopupRequest = true
                    self.puchController(controller: VC)
                }else {
                    self.showAlert(title: "Error", message: response.message ?? "", action1Name: "Ok", action2Name: nil)
                }
                
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func paypalAction(_ sender: Any) {
        self.configurePayPalCheckout()
    }
    
}

extension SettingsViewController {
    func configurePayPalCheckout() {
        
        Checkout.start(
            createOrder: { createOrderAction in
                
                let amount = PurchaseUnit.Amount(currencyCode: .usd, value: "10.00")
                let purchaseUnit = PurchaseUnit(amount: amount)
                let order = OrderRequest(intent: .capture, purchaseUnits: [purchaseUnit])
                
                createOrderAction.create(order: order)
                
            }, onApprove: { approval in
                
                approval.actions.capture { (response, error) in
                    self.showAlert(title: "Order successfully captured: \(response?.data)", message: nil, action1Name: "Ok", action2Name: nil)
                    print("Order successfully captured: \(response?.data)")
                }
                
            }, onCancel: {
                print("Order cancel:")
                // Optionally use this closure to respond to the user canceling the paysheet
                
            }, onError: { error in
                print(error)
                // Optionally use this closure to respond to the user experiencing an error in
                // the payment experience
                
            }
        )
        
        Checkout.setOnApproveCallback { approval in
            approval.actions.capture { (response, error) in
                print("Order successfully captured: \(response?.data)")
            }
        }
        
        Checkout.setOnCancelCallback {
            print("caleed")
        }
        
        Checkout.setOnErrorCallback { error in
            print("caleed")
        }
        
    }
    
    
}

extension SettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayContent.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayContent[section].content!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(arrayContent[indexPath.section].content![indexPath.row].name == ""){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell2", for: indexPath) as! SettingTableViewCell
            cell.labelText.text = "Earn $ " + K_REFER_AMOUNT
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
            
            let content = arrayContent[indexPath.section].content![indexPath.row]
            cell.icon.image = content.icon
            cell.labelText.text = content.name
            
            if content.name == "Passcode / Touch ID" {
                cell.labelRightText.isHidden = true
                cell.settingSwitch.isHidden = false
                cell.settingSwitch.setOn(Singleton.shared.enableAuthentication, animated: false)
            } else if content.name == "Currency" {
                cell.labelRightText.text = Singleton.shared.selectedCurrency.description
                cell.labelRightText.isHidden = false
                cell.settingSwitch.isHidden = true
            } else if content.name == "Accounts" {
                cell.labelRightText.text = DBManager.sharedInstance.currentUser[0].accNumber
                cell.labelRightText.isHidden = false
                cell.settingSwitch.isHidden = true
            } else if content.name == "Delegate Server" {
                cell.labelRightText.text = self.selectedServer
                cell.labelRightText.isHidden = false
                cell.settingSwitch.isHidden = true
            } else {
                cell.labelRightText.isHidden = true
                cell.settingSwitch.isHidden = true
            }
            
            cell.valueChanged = {
                if cell.settingSwitch.isOn {
                    Singleton.shared.enableAuthentication = true
                    self.arrayContent[indexPath.section].content?.append(Settings(icon: #imageLiteral(resourceName: "settings-1"), name: "Advanced Settings"))
                    tableView.insertRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                    Singleton.shared.autoLockTimer = AutoLock.immediate.interval
                } else {
                    Singleton.shared.enableAuthentication = false
                    self.arrayContent[indexPath.section].content?.remove(at: 1)
                    tableView.deleteRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                }
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrayContent[section].title
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let content = arrayContent[indexPath.section].content![indexPath.row]
        switch content.name {
        case "":
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferEarnViewController") as! ReferEarnViewController
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Accounts":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            puchController(controller: VC)
            break
        case "Swap":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SwapViewController") as! SwapViewController
            puchController(controller: VC)
            break
        case "Logout":
            self.showPopup(title: "Logout", msg: "Are you sure?")
            break
        case "Passcode / Touch ID":
            break
        case "Private Key":
           // self.showPopUp()
            let passcodeController = self.storyboard?.instantiateViewController(withIdentifier: "EnterPasscodeViewController") as! EnterPasscodeViewController
            passCodeScreen = "PrivateKey"
            self.navigationController?.pushViewController(passcodeController, animated: false)
            break
        case "KYC":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "KYCViewController") as! KYCViewController
            puchController(controller: VC)
            break
        case "Advanced Settings":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SubSettingContentViewController") as! SubSettingContentViewController
            
            VC.navBarTitle = content.name
            VC.arrayContent = [SettingsHeading(title: "Advanced Settings", content: [Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), name: "Auto-Lock")])]
            
            puchController(controller: VC)
            break
        case "Currency":
            var currencyContent: [Currency] = []
            
            for code in NSLocale.isoCurrencyCodes as [String] {
                let locale = NSLocale(localeIdentifier: Locale.current.identifier  )
                if let name = locale.displayName(forKey: .currencyCode, value: code), let symbol = locale.displayName(forKey: .currencySymbol, value: code), !(name.contains("(")) {
                    currencyContent.append(Currency(description: "\(code) - \(name)", code: code, symbol: symbol, isSelected: false))
                }
            }
            
            print(currencyContent)
            
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SubSettingContentViewController") as! SubSettingContentViewController
            
            VC.navBarTitle = content.name
            VC.currencyList = currencyContent
            
            puchController(controller: VC)
            break
        case "Transactions":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            puchController(controller: VC)
            break
        case "Cashback Rewards":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
            puchController(controller: VC)
            break
        case "Topup Requests":
            if let token = UserDefaults.standard.value(forKey: K_BTE_CRYPTO_TOKEN) as? String{
                let VC = self.storyboard?.instantiateViewController(withIdentifier: "RewardsViewController") as! RewardsViewController
                VC.isTopupRequest = true
                puchController(controller: VC)
                
            }else {
                self.popupView.isHidden = false
            }
            break
            
            //        case "Swap":
            //            let VC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            //            puchController(controller: VC)
            //            break
        case "Notifications":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            puchController(controller: VC)
            break
        case "Twitter":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://twitter.com/Barteos_org")
            break
        case "Linkedin":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://au.linkedin.com/company/barteos")
            break
        case "Facebook":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://www.facebook.com/barteos.org/")
            break
        case "Support":
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
            self.navigationController?.pushViewController(myVC, animated: true)
            break
        case "Delegate Server":
            
            self.firstServerView.backgroundColor = selectedServer == "BTE AUS" ? primaryColor:.lightGray
            self.secondServerView.backgroundColor = selectedServer == "BTE AUS" ? .lightGray:primaryColor
            self.serverView.isHidden = false
            break
        default:
            print("default")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func showPopup(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            
            UserDefaults.standard.removeObject(forKey: K_TOKEN)
            UserDefaults.standard.removeObject(forKey: K_BALANCE)
            UserDefaults.standard.removeObject(forKey: K_TOKEN_BALANCE)
            UserDefaults.standard.removeObject(forKey: K_BTE_CRYPTO_TOKEN)
            DBManager.sharedInstance.deleteAllFromDB()
            let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
            splashController.showCancel = false
            SinglePageViewController.controller = K_SPLASH
            self.puchController(controller: splashController)
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
}

extension SettingsViewController {
    func downloadReceipt(url: String) {
        ActivityIndicator.show(view: self.view)
        self.showAlert(title: "Downloadinf pdf", message: nil, action1Name: "Ok", action2Name: nil)
        if let url = URL(string: url){
            
            let fileName = String((url.lastPathComponent)) as NSString
            // Create destination URL
            let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
            //Create URL to the source file you want to download
            let fileURL = url
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL)
             let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                  //  (sender as? UIButton)?.isUserInteractionEnabled = true
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        ActivityIndicator.hide()
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        do {
                            self.showAlert(title: "Successfully saved private key.", message: nil, action1Name: "Ok", action2Name: nil)
                            
                            //Show UIActivityViewController to save the downloaded file
                            let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                            ActivityIndicator.hide()
                            DispatchQueue.main.async {
                            self.saveFile(contents: contents,dUrl: destinationFileUrl)
                            }
                        }
                        catch (let err) {
                            ActivityIndicator.hide()
                          //  (sender as? UIButton)?.isUserInteractionEnabled = true
                         //    Singleton.shared.showToast(text: "Error downloading file")
                            print("error: \(err)")
                        }
                    } catch (let writeError) {
                        ActivityIndicator.hide()
                     //  (sender as? UIButton)?.isUserInteractionEnabled = true
                      //  Singleton.shared.showToast(text: "File already exist")
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                } else {
                    ActivityIndicator.hide()
                 //  (sender as? UIButton)?.isUserInteractionEnabled = true
                   // Singleton.shared.showToast(text: "Error downloading file")
                    print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
                }
            }
            task.resume()
        }else {
            ActivityIndicator.hide()
            self.showAlert(title: "No URL Found.", message: nil, action1Name: "Ok", action2Name: nil)
        }
    }
    
    func saveFile(contents: [URL],dUrl:URL){
       
        for indexx in 0..<contents.count {
            if contents[indexx].lastPathComponent == dUrl.lastPathComponent {
                let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                self.navigationController?.present(activityViewController, animated: true, completion: nil)
            }
        }

    }
}
