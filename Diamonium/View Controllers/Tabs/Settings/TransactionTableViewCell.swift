//
//  TransactionTableViewCell.swift
//  Diamonium
//
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    
    //MARK: IBOutlet
    @IBOutlet weak var profileImage: ImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var bteLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
