//
//  AutoLock.swift
//  Diamonium
//
//

import UIKit

enum AutoLock: Int {
    case immediate
    case oneMinute
    case fiveMinute
    case oneHour
    case fiveHours
    
    var name: String {
        switch self {
        case .immediate:
            return "Immediate"
        case .oneMinute:
            return "If away for 1 minute"
        case .fiveMinute:
            return "If away for 5 minute"
        case .oneHour:
            return "If away for 1 hour"
        case .fiveHours:
            return "if away for 5 hours"
        }
    }
    
    var interval: TimeInterval {
        switch self {
        case .immediate:
            return 0
        case .oneMinute:
            return 60
        case .fiveMinute:
            return 300
        case .oneHour:
            return 3600
        case .fiveHours:
            return 18000
        }
    }
}
