//
//  TopupViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 27/08/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit
import WebKit
import LocalAuthentication
import SkyFloatingLabelTextField
import PayPalCheckout

class TopupViewController: UIViewController, CustomPickerDelegate, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == selectedCrytoValue){
            var amount = (self.selectedCrytoValue.text ?? "") + string
            amount = amount.replacingOccurrences(of: ",", with: "")
            if(string == ""){
                amount = String(amount.dropLast())
            }
            if(amount == "0" || amount == "0.0" || amount == "0.00" || amount == ""){
                self.bteValue.text = ""
                self.audValue.text = ""
                return true
            }
            param = [
                "preferred_payment": self.pickerValue.text,
                "amount_type" : self.pickerValue.text,
                "amount" : amount
            ]
        }else {
            var amount = (self.bteValue.text ?? "") + string
            if(string == ""){
                amount = String(amount.dropLast())
            }
            if(amount == "0" || amount == "0.0" || amount == "0.00" || amount == ""){
                self.selectedCrytoValue.text = ""
                return true
            }else if(self.pickerValue.text == "BTC" && Double(amount ?? "")!<100){
                self.selectedCrytoValue.text = ""
                return true
            }else if(self.pickerValue.text == "ETH" && Double(amount ?? "")!<10){
                self.selectedCrytoValue.text = ""
                return true
            }
            
            param = [
                "preferred_payment":self.pickerValue.text,
                "amount_type" :  "BTE",
                "amount" : amount
            ]
        }
        self.convertBteToCrypto()
        
        return true
    }
    
    func getSelectedData(value: String) {
        if(self.dropdownType == 1){
            self.cryptoLabel.text = value.uppercased()
            self.pickerValue.text = value.uppercased()
            self.isBteRate = true
            self.selectedCrytoValue.text = ""
            self.bteLabel.text = "BTE"
            self.bteValue.text = ""
            self.audValue.text = ""
            self.packageLabel.text = ""
            self.rewardField.text = ""
            if(self.cryptoLabel.text == "DIRECT_DEPOSIT" || self.cryptoLabel.text == "PAYPAL"){
                self.topupDefaultView.isHidden = true
                self.rewardView.isHidden = false
            }else {
                self.topupDefaultView.isHidden = false
                self.rewardView.isHidden = true
            }
        }else {
            self.packageLabel.text = value.uppercased()
            let package = self.packageData.filter({ val in
                val.title == value
            })
            if(package.count > 0){
                self.rewardField.text = package[0].amount ?? ""
            }else {
                self.rewardField.text = ""
            }
            
        }
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var cryptoLabel: UILabel!
    @IBOutlet weak var selectedCrytoValue: UITextField!
    @IBOutlet weak var bteLabel: UILabel!
    @IBOutlet weak var bteValue: UITextField!
    @IBOutlet weak var pickerValue: UILabel!
    @IBOutlet weak var btePrice: UILabel!
    
    @IBOutlet weak var popupView: View!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    @IBOutlet weak var successView: View!
    
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var preferredPaymentLabel: UILabel!
    @IBOutlet weak var amountTypeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var clipboard: View!
    
    @IBOutlet weak var audValue: UITextField!
    @IBOutlet weak var audLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    
    @IBOutlet weak var rewardView: UIStackView!
    @IBOutlet weak var topupDefaultView: UIStackView!
    @IBOutlet weak var rewardField: UITextField!
    
    
    @IBOutlet weak var bankView: UIStackView!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var bankAccountNumber: UILabel!
    @IBOutlet weak var bankCode: UILabel!
    
    
    var packageData = [PackageResponse]()
    var dropdownData = [String]()
    var isBteRate = false
    var param = [String:Any]()
    var dropdownType = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bteValue.delegate = self
        selectedCrytoValue.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.getAvailableCrypto()
            
        }
        self.getPackagesData()
        self.audLabel.text = Singleton.shared.selectedCurrency.code
        self.btePrice.text = "0" + (Singleton.shared.bteFeedLastData.bte ?? "")
    }
    
    func getPackagesData(){
        SessionManager.shared.methodForApiCalling(url:U_BASE + U_GET_PACKAGES, method: .get, parameter: nil, objectClass: [PackageResponse].self, requestCode: U_GET_PACKAGES, userToken: nil) { response in
            self.packageData = response
        }
    }
    
    func getAvailableCrypto(){
        if(self.dropdownData.count == 0){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url:U_BASE + U_GET_SUPPORTED_CRYPTOS, method: .get, parameter: nil, objectClass: GetSupportedCrypto.self, requestCode: U_GET_SUPPORTED_CRYPTOS, userToken: nil) { response in
                self.dropdownData = response.cryptos
                self.btePrice.text = "0" + (Singleton.shared.bteFeedLastData.bte ?? "")
                ActivityIndicator.hide()
            }
        }
    }
    
    func convertBteToCrypto(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_CALCULATE_BTE_FROM_CRYPTO, method: .post, parameter: self.param, objectClass: CalculateBteFromCrypto.self, requestCode: U_CALCULATE_BTE_FROM_CRYPTO, userToken: nil) { response in
            if((self.param["amount_type"] as? String) == "BTE") {
                self.selectedCrytoValue.text = "\(response.preferred_payable_amount ?? 0)"
                self.bteValue.text = "\(response.amount ?? "")"
                self.apiCallToConvertCurrency(response.amount ?? "")
            }else {
                self.selectedCrytoValue.text = "\(response.amount ?? "")"
                self.bteValue.text = "\(response.bte_amount ?? 0)"
                self.apiCallToConvertCurrency("\(response.bte_amount ?? 0)")
            }
            
        }
    }
    
    func apiCallToConvertCurrency(_ amount: String) {
        if(Double(amount.replacingOccurrences(of: ",", with: "")) != 0){
            ActivityIndicator.show(view: self.view)
            var param = [String:Any]()
            param = [K_AMOUNT: Double(amount.replacingOccurrences(of: ",", with: "")),
              K_CURRENCY_FROM: "BTE",
                K_CURRENCY_TO: Singleton.shared.selectedCurrency.code]
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
                self.audValue.text = "\(balance.local_value ?? 0)"
                ActivityIndicator.hide()
            }
        }
    }
    
    func callTopupApi(token:String){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_PURCHASE_BTE_CRYPTOS, method: .post, parameter: self.param, objectClass: BteFromCryptoResponse.self, requestCode: U_PURCHASE_BTE_CRYPTOS, userToken: token) { response in
            self.bankView.isHidden = true
            self.clipboard.isHidden = false
            self.loginView.isHidden = true
            self.orderIdLabel.text = response.order_id
            self.addressLabel.text = response.payment_address
            self.confirmationLabel.text = "Please pay \(self.selectedCrytoValue.text ?? "") \(self.cryptoLabel.text ?? "") to above address"
            self.preferredPaymentLabel.text = self.cryptoLabel.text
            self.amountTypeLabel.text = "BTE"
            self.amountLabel.text = self.bteValue.text
            self.successView.isHidden = false
            self.popupView.isHidden = false
            self.bteValue.resignFirstResponder()
            ActivityIndicator.hide()
        }
    }
    
    func callRewardApi(token:String){
        ActivityIndicator.show(view: self.view)
        let package = self.packageData.filter { val in
            val.title == self.packageLabel.text
        }
        let param = [
            "preferred_payment": self.pickerValue.text,
            "amount_type" : self.pickerValue.text,
            "amount" : package.count > 0 ? package[0].amount: "0"
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_PURCHASE_BTE_CRYPTOS, method: .post, parameter: param, objectClass: BteFromCryptoResponse.self, requestCode: U_PURCHASE_BTE_CRYPTOS, userToken: token) { response in
            self.bankView.isHidden = self.pickerValue.text != "DIRECT_DEPOSIT"
            self.loginView.isHidden = true
            self.clipboard.isHidden = true
            self.orderIdLabel.text = response.order_id
            self.addressLabel.text = response.payment_address
            self.confirmationLabel.text = "Please pay $\(self.selectedCrytoValue.text ?? "") to above address"
            self.preferredPaymentLabel.text = self.cryptoLabel.text
            self.amountTypeLabel.text = "BTE"
            self.amountLabel.text = "$\(package.count > 0 ? package[0].amount ?? "": "0")"
            self.successView.isHidden = false
            self.popupView.isHidden = false
            self.bteValue.resignFirstResponder()
            self.configurePayPal(amount: package.count > 0 ? package[0].amount ?? "": "0")
            ActivityIndicator.hide()
        }
    }
    
    func configurePayPal(amount: String) {
        if(self.pickerValue.text == "PAYPAL"){
            
            Checkout.start(
                createOrder: { createOrderAction in
                    let amount = PurchaseUnit.Amount(currencyCode: .usd, value: amount)
                    let purchaseUnit = PurchaseUnit(amount: amount)
                    let order = OrderRequest(intent: .capture, purchaseUnits: [purchaseUnit])
                    
                    createOrderAction.create(order: order)
                    
                }, onApprove: { approval in
                    
                    approval.actions.capture { (response, error) in
                        self.showAlert(title: "Order successfully captured: \(response?.data)", message: nil, action1Name: "Ok", action2Name: nil)
                        print("Order successfully captured: \(response?.data)")
                    }
                    
                }, onCancel: {
                    print("Order cancel:")
                    // Optionally use this closure to respond to the user canceling the paysheet
                    
                }, onError: { error in
                    print(error)
                    // Optionally use this closure to respond to the user experiencing an error in
                    // the payment experience
                    
                }
            )
            
            Checkout.setOnApproveCallback { approval in
                approval.actions.capture { (response, error) in
                    print("Order successfully captured: \(response?.data)")
                }
            }
            
            Checkout.setOnCancelCallback {
                print("caleed")
            }
            
            Checkout.setOnErrorCallback { error in
                print("caleed")
            }
            
        }
    }
    
    //MARK: IBActions
    @IBAction func dropdownAction(_ sender: UIButton) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomPickerViewController") as! CustomPickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        self.dropdownType = sender.tag
        if(self.dropdownType == 2){
            myVC.arrayCryptos = self.packageData.map({ val in
                return val.title ?? ""
            })
        }else {
            myVC.arrayCryptos = dropdownData
        }
        myVC.customPickerdelegate = self
        self.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func switchCurrencyAction(_ sender: Any) {
        self.isBteRate = !self.isBteRate
        self.bteLabel.text = self.isBteRate ? self.pickerValue.text:"BTE"
        self.cryptoLabel.text = self.isBteRate ? "BTE":self.pickerValue.text
        self.bteValue.text = ""
        self.selectedCrytoValue.text = ""
        self.audValue.text = ""
    }
    
    
    @IBAction func transferAction(_ sender: Any) {
        if(self.selectedCrytoValue.text!.isEmpty && self.rewardView.isHidden){
            self.showAlert(title: "Error", message: "Enter BTE value", action1Name: "Ok", action2Name: nil)
        }else if(self.packageLabel.text!.isEmpty && self.topupDefaultView.isHidden){
            self.showAlert(title: "Error", message: "Select package", action1Name: "Ok", action2Name: nil)
        }else {
            if let token = UserDefaults.standard.value(forKey: K_BTE_CRYPTO_TOKEN) as? String{
                if(self.topupDefaultView.isHidden){
                  self.callRewardApi(token: token)
                }else {
                    self.callTopupApi(token:token)
                }
            }else if(DBManager.sharedInstance.currentUser[0].email!.isEmpty){
                self.showAlert(title: "Error", message: "Please complete your profile first", action1Name: "Ok", action2Name: nil)
            }else {
                self.emailField.text = ""
                self.passwordField.text = ""
                self.emailField.text = DBManager.sharedInstance.currentUser[0].email ?? ""
                self.loginView.isHidden = false
                self.successView.isHidden = true
                self.popupView.isHidden = false
            }
        }
    }
    
    @IBAction func generateTokenAction(_ sender: Any) {
        if(self.emailField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Email Address", action1Name: "Ok", action2Name: nil)
        }else if(self.passwordField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GENERATE_TOKEN, method: .post, parameter: ["email":self.emailField.text, "password":self.passwordField.text], objectClass: RegisterUser.self, requestCode: U_GENERATE_TOKEN, userToken: nil) { response in
                ActivityIndicator.hide()
                self.popupView.isHidden = true
                if(response.token != "" || response.token != nil){
                    UserDefaults.standard.setValue(response.token ?? "", forKey: K_BTE_CRYPTO_TOKEN)
                    if(self.topupDefaultView.isHidden){
                      self.callRewardApi(token: response.token ?? "")
                    }else {
                        self.callTopupApi(token:response.token ?? "")
                    }
                }else {
                    self.showAlert(title: "Error", message: response.message ?? "", action1Name: "Ok", action2Name: nil)
                }
                
            }
        }
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: self.addressLabel.text ?? "")
    }
    
    
}
