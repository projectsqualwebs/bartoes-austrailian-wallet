//
//  SupportViewController.swift
//  Diamonium
//
//  Created by AM on 17/09/19.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class SupportViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var fullName: SkyFloatingLabelTextField!
    @IBOutlet weak var supportTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberField: SkyFloatingLabelTextField!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var documentimage: UIImageView!
    @IBOutlet weak var documentButton: UIButton!
    
    var imageName: String = ""
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.documentButton.setImage(UIImage(named: "copy"), for: .normal)
        if  (DBManager.sharedInstance.currentUser.count > 0) {
            self.email.text = DBManager.sharedInstance.currentUser[0].email
            self.fullName.text = DBManager.sharedInstance.currentUser[0].name
            self.phoneNumberField.text = DBManager.sharedInstance.currentUser[0].phone
        }
    }
    
    @IBAction func submitForm(_ sender: Any) {
        if(self.email.text!.isEmpty){
            self.showAlert(title: nil, message: "Email address required", action1Name: "Ok", action2Name: nil)
        }else if !(self.isValidEmail(emailStr: self.email.text!)){
            self.showAlert(title: nil, message: "Enter valid email address", action1Name: "Ok", action2Name: nil)
        }else  if(self.fullName.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter your full name", action1Name: "Ok", action2Name: nil)
        }else  if(self.phoneNumberField.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter your phone number", action1Name: "Ok", action2Name: nil)
        }else  if(self.supportTitle.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter title", action1Name: "Ok", action2Name: nil)
        }else  if(self.desc.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter description", action1Name: "Ok", action2Name: nil)
        }else  if(self.desc.text!.count < 20){
            self.showAlert(title: nil, message: "Enter atleast 20 characters", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
        
            let supportTitle = self.supportTitle.text
            let imageData = self.documentimage.image?.jpegData(compressionQuality: 0.7)
            let myImage = imageData?.base64EncodedString()


            let descript = self.desc.text
            let mail = self.email.text
            let name = self.fullName.text!
            let token = UserDefaults.standard.string(forKey: K_TOKEN)
            let headers: HTTPHeaders = [
                "X-API-Key": "E079C1CB29ED892EFA141B16D69E4C1E",
                "Authorization": "Bearer " + (token ?? "")
            ]
            let param:[String:Any] = [
                "email": mail,
                "phone": self.phoneNumberField.text ?? "",
                "name": name,
                "subject": supportTitle,
                "message": descript,
                "topicId": 13,
                "attachments": [
                    ["image.png":myImage]
                ]
            ]

            Alamofire.request(U_CREATE_SUPPORT_TICKET, method: .post, parameters: param, encoding: JSONEncoding.default, headers:headers).responseString { (dataResponse) in
                let statusCode = dataResponse.response?.statusCode
                switch dataResponse.result {
                case .success(_):
                  //  let object = SessionManager.convertDataToObject(response: dataResponse.data, Response.self)
                   // let errorObject = SessionManager.convertDataToObject(response: dataResponse.data, Response.self)

                    if (statusCode == 200 || statusCode == 201){
                        self.supportTitle.text = ""
                        self.desc.text = ""
                        self.documentimage.image = nil
                         self.documentButton.setImage(UIImage(named: "copy"), for: .normal)
                        let object = SessionManager.shared.convertDataToObject(response: dataResponse.data, Response.self)
                          UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Ticket has been created.", message: object?.message, action1Name: "Ok", action2Name: nil)
                    } else if statusCode == 404 {
                        self.showAlert(title: "Error", message: "Error submitting response", action1Name: "ok", action2Name: nil)
                        print("errorObject?.message")
                    } else {
                        self.showAlert(title: "Error", message: "Error submitting response", action1Name: "ok", action2Name: nil)
                      //  print(errorObject?.message)
                    }
                    ActivityIndicator.hide()
                    break
                case .failure(_):
                    ActivityIndicator.hide()
                    self.showAlert(title: "Error", message: "Error submitting response", action1Name: "ok", action2Name: nil)
                    break
                }
            }
        }
    }
    
    
    
    @IBAction func selectFile(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
               alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                   self.openCamera()
               }))
               
               alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                   self.openGallary()
               }))
               
               alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
               if let popoverController = alert.popoverPresentationController {
                   popoverController.sourceView = sender as! UIView
                   popoverController.sourceRect = (sender as AnyObject).bounds
               }
               self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SupportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate {
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
        } else {
            self.imageName = "image.jpg"
        }
        self.documentimage.image = selectedImage
        self.documentButton.setImage(nil, for: .normal)
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
