//
//  AmountViewController.swift
//  Diamonium
//
//

import UIKit
import LocalAuthentication
import Lottie



class AmountViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == self.coupenField){
            self.coupenNote.text = ""
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == self.coupenField){
            if(textField.text == ""){
                self.coupenNote.text = ""
            }
        }
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var labelBalance1: UILabel!
    @IBOutlet weak var labelBalanceTypeFrom: UILabel!
    @IBOutlet weak var labelBalance2: UILabel!
    @IBOutlet weak var labelBalanceTypeTo: UILabel!
    @IBOutlet weak var labelBalanceNow: UILabel!
    @IBOutlet weak var labelBalAfter: UILabel!
    
    @IBOutlet weak var buttonForCancel: UIButton!
    @IBOutlet weak var transferMainPopUp: UIView!
    
    @IBOutlet weak var finalPreviewPopUp: View!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var labelTo: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var memoLabel: UIButton!
    
    @IBOutlet weak var coupenField: UITextField!
    @IBOutlet weak var coupenNote: UILabel!
    @IBOutlet weak var confirmationCoupenNote: UILabel!
    
    @IBOutlet weak var transferConfirmPopUp: View!
    @IBOutlet weak var labeConfirmFrom: UILabel!
    @IBOutlet weak var labelConfirmTo: UILabel!
    @IBOutlet weak var labelConfirmAmount: UILabel!
    @IBOutlet weak var labelBlockNumber: UILabel!
    
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var gstTick: ImageView!
    
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var switchViewBottom: UIView!
    @IBOutlet weak var switchValueBottom: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var transferFeeLabel: UILabel!
    
    var user: DBUsers?
    var transferTo: String?
    var beValue: String?
    var localValue: String?
    var showBTE: Bool = false
    var amount: Double = 0
    var userToken: String?
    var memoText: String?
    var isDecimal = false
    var countDecimal = 0
    var counpenData = CoupenResponse()
    
    var selectedCoupen:String?
    var selecteBte:String?
    var selectedGst: String?
    var unclearedAmount = GetUnclearedAmount()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coupenField.delegate = self
        
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.play()
        
        transferMainPopUp.alpha = 0
        userToken = user?.token
        
        self.getCurrentBalance()
    }
    
    func getCurrentBalance(){
        if(DBManager.sharedInstance.currentUser.count > 0){
            SessionManager.shared.methodForApiCalling(url: U_GET_CLEARED_AMOUNT + "\(self.user?.accNumber ?? "")", method: .get, parameter: nil, objectClass: GetUnclearedAmount.self, requestCode: U_GET_CLEARED_AMOUNT, userToken: nil) { response in
                self.unclearedAmount = self.getValueRemovingComma(response: response)
                self.setInitialValues()
            }
        }
    }
    
    func setInitialValues(){
        if ((beValue ?? "") != "") {
            amount = Double(beValue ?? "0")!
            labelBalance1.text = localValue
            labelBalance2.text = beValue
            labelBalanceTypeFrom.text = user?.currencyCode
            if((self.selectedCoupen ?? "") != ""){
                self.coupenField.text = selectedCoupen
                self.applyAction(self)
            }
            if(selecteBte == "BTE" || selecteBte == "BTEAUD"){
                self.labelBalanceTypeFrom.isHidden = false
                self.switchView.isHidden = true
                self.switchViewBottom.isHidden = false
                self.switchLabel.text = selecteBte == "BTE" ? "BTE":"BTE-AUD"
                self.switchValueBottom.text = selecteBte == "BTE" ? "BTE":"BTE-AUD"
                self.labelBalanceTypeTo.isHidden = true
            }
            
            if(self.selectedGst == "1"){
                self.gstTick.image = UIImage(named: "checkmark")
                
            }
            var balAfter = String()
            if(selecteBte == "BTE"){
                balAfter = String(format: "%.2f", ((Double(user!.bteBalance!)!) - (K_TRANSFER_FEES + (Double(beValue!)!))))
                labelBalanceNow.text = "BALANCE NOW: " + (user?.bteBalance)! + " \(self.switchLabel.text ?? "")"
            }else {
                balAfter = String(format: "%.2f", ((Double(self.unclearedAmount.available_bte_aud ?? "0")!) - (K_TRANSFER_FEES + (Double(beValue!)!))))
                labelBalanceNow.text = "BALANCE NOW: " + (self.unclearedAmount.available_bte_aud ?? "0") + " \(self.switchLabel.text ?? "")"
            }
            labelBalAfter.text = "BALANCE AFTER: " + balAfter + " \(self.switchLabel.text ?? "")"
            transferFeeLabel.text = "0.01 \(self.switchLabel.text ?? "") TXN FEE"
        } else {
            handleBalance(switchCurrency: false)
        }
    }
    
    func handleBalance(switchCurrency: Bool) {
        if((self.switchLabel.text ?? "") == "BTE"){
            labelBalanceNow.text = "BALANCE NOW: " + (user?.bteBalance)! + " \(self.switchLabel.text ?? "")"
            labelBalAfter.text = "BALANCE AFTER: " + (user?.bteBalance)! + " \(self.switchLabel.text ?? "")"
        }else {
            labelBalanceNow.text = "BALANCE NOW: " + (self.unclearedAmount.available_bte_aud ?? "0") +  " \(self.switchLabel.text ?? "")"
            labelBalAfter.text = "BALANCE AFTER: " + (self.unclearedAmount.available_bte_aud ?? "0") +  " \(self.switchLabel.text ?? "")"
        }
        
        transferFeeLabel.text = "0.01 \(self.switchLabel.text ?? "") TXN FEE"
        
        labelBalance1.text = "0"
        if showBTE {
            labelBalanceTypeFrom.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = user?.currencyCode
            
            self.labelBalanceTypeFrom.isHidden = true
            self.switchView.isHidden = false
            self.switchViewBottom.isHidden = true
            self.switchLabel.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            self.switchValueBottom.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            self.labelBalanceTypeTo.isHidden = false
        } else {
            labelBalanceTypeFrom.text = user?.currencyCode
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            
            self.labelBalanceTypeFrom.isHidden = false
            self.switchView.isHidden = true
            self.switchViewBottom.isHidden = false
            self.switchLabel.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            self.switchValueBottom.text = ((self.switchLabel.text ?? "") == "BTE") ? "BTE":"BTE-AUD"
            self.labelBalanceTypeTo.isHidden = true
        }
    }
    
    func showPopUp(view: UIView, fundTransfer: FundTransfer?) {
        if view == finalPreviewPopUp {
            buttonForCancel.isHidden = false
            finalPreviewPopUp.isHidden = false
            if((self.selectedGst ?? "")==""){
                self.gstTick.image = nil
            }
            if((self.selectedCoupen ?? "") == ""){
                self.coupenNote.text = ""
                self.coupenField.text = ""
            }
            transferConfirmPopUp.isHidden = true
            labelFrom.text = user?.accNumber
            labelTo.text = transferTo
            labelAmount.text = amount.description + " \(self.switchLabel.text ?? "")"
        } else {
            buttonForCancel.isHidden = true
            finalPreviewPopUp.isHidden = true
            transferConfirmPopUp.isHidden = false
            labeConfirmFrom.text = fundTransfer?.name
            labelConfirmTo.text = fundTransfer?.to_name
            labelConfirmAmount.text = "\(fundTransfer!.amount!) \(self.switchLabel.text ?? "")"
        }
        
        transferMainPopUp.alpha = 0.3
        view.alpha = 0.5
        view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        
        UIView.animate(withDuration: 0.5) {
            self.transferMainPopUp.alpha = 1
            view.alpha = 1
            view.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    @IBAction func cancelFinalPreviewAction(_ sender: Any) {
        self.transferMainPopUp.alpha = 0
        finalPreviewPopUp.isHidden = true
        transferConfirmPopUp.isHidden = true
    }
    
    
    @IBAction func addMemoAction(_ sender: Any) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addMemotText(notification:)), name: Notification.Name("MemoText"), object: nil)
        let myVC =
        self.storyboard?.instantiateViewController(withIdentifier: "AddMemoViewController") as! AddMemoViewController
        myVC.text = self.memoText ?? ""
        self.present(myVC, animated: true, completion: nil)
    }
    
    @objc func addMemotText(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("MemoText"), object: nil)
        let dict = notification.object as! NSDictionary
        if( dict["message"] as! String != ""){
            self.memoLabel.setTitle("Edit Memo", for: .normal)
        }
        self.memoText = dict["message"] as! String
    }
    
    
    func hidePopUp(view: UIView, popBack: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.transferMainPopUp.alpha = 0
            view.alpha = 0
            view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        }) { (_) in
            if popBack {
                NotificationCenter.default.post(name: NSNotification.Name("refreshFields"), object: nil)
                self.popToTabController()
            }
        }
    }
    
    func addTransactionToList(fund:FundTransfer) {
        let timeInterval: Int = Int(Date().timeIntervalSince1970)
        DBManager.sharedInstance.addTransaction(trxList: [Transaction(id: fund.trxid, name: fund.to_name, dateTime: timeInterval, amount: "\(self.amount)", type: K_SENT, blockId:fund.block_id!,memoText:self.memoText,to_name: fund.to_name,authorizer: fund.authorizer, asset: fund.asset ?? "")], accNumber: fund.name)
    }
    
    func apiCallToTransfer() {
        var param = [String:Any]()
        let code = self.coupenField.text ?? ""
        let isGst = self.gstTick.image == UIImage(named: "checkmark") ? 1:0
        if(self.counpenData.data.discount_amount != nil){
            param = [K_ASSET: self.switchValueBottom.text == "BTE" ? "bte":"bteaud" , K_AMOUNT: amount.description, K_TO: transferTo!, K_MEMO: "", "coupon_code": code,"gst_included":isGst]
        }else {
            param = [K_ASSET: self.switchValueBottom.text == "BTE" ? "bte":"bteaud" , K_AMOUNT: amount.description, K_TO: transferTo!, K_MEMO: "","gst_included":isGst]
            self.confirmationCoupenNote.isHidden = true
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_TRANSFER_MONEY, method: .post, parameter: param, objectClass: SendMoney.self, requestCode: U_TRANSFER_MONEY, userToken: self.userToken) { (fundTransfer) in
            if(self.counpenData.data.discount_amount != nil){
                self.confirmationCoupenNote.isHidden = false
                self.confirmationCoupenNote.text = self.coupenNote.text
            }
            self.gstTick.image = nil
            self.coupenNote.text = ""
            self.coupenField.text = ""
            self.labelBlockNumber.text = "\(fundTransfer.trxData.block_id!)"
            self.addTransactionToList(fund: fundTransfer.trxData)
            self.memoLabel.setTitle("Add Memo", for: .normal)
            SessionManager.shared.apiCallToGetBalance(completionHandler: { (balance, tokenBalance, beReward)  in
                DispatchQueue.main.async {
                    DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance, beRewards: beReward)
                    
                    self.showPopUp(view: self.transferConfirmPopUp, fundTransfer: fundTransfer.trxData)
                    NotificationCenter.default.post(name: NSNotification.Name("updateBalance"), object: self.userToken)
                }
                
            })
        }
    }
    
    func apiCallToConvertCurrency() {
        var param = [String:Any]()
        if(showBTE){
            param = [K_AMOUNT: Double((self.labelBalance1.text ?? "0")!),
              K_CURRENCY_FROM: (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: ""),
                K_CURRENCY_TO: labelBalanceTypeTo.text ?? ""]
        }else {
            param = [K_AMOUNT: Double((self.labelBalance1.text ?? "0")!),
              K_CURRENCY_FROM: labelBalanceTypeFrom.text ?? "",
                K_CURRENCY_TO: (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: "")
            ]
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
            var bteBalance = Double()
            if(self.switchLabel.text == "BTE"){
                bteBalance =  Double((self.user?.bteBalance ?? "0")!)! - (balance.be_value ?? 0) - K_TRANSFER_FEES
            }else{
                bteBalance =  Double((self.unclearedAmount.available_bte_aud ?? "0"))! - (balance.be_value ?? 0) - K_TRANSFER_FEES
            }
            
            let currencyBalance = String(format: "%.2f", bteBalance)
            self.amount = Double(String(format: "%.2f", (balance.be_value) ?? 0))!
            self.labelBalAfter.text = "BALANCE AFTER: " + currencyBalance + " \(self.switchLabel.text ?? "")"
            
            if !(self.showBTE) {
                if (self.labelBalance1.text == "0"){
                    self.labelBalance2.text = "0"
                }else{
                    self.labelBalance2.text = String(format: "%.2f", (balance.be_value) ?? 0)
                }
            } else {
                if (self.labelBalance1.text == "0"){
                    self.labelBalance2.text = "0"
                }else{
                    self.labelBalance2.text = String(format: "%.2f", (balance.local_value) ?? 0)
                }
            }
            ActivityIndicator.hide()
        }
        
    }
    
    //MARK: Action
    @IBAction func customKeyboard(_ sender: UIButton) {
    
            let value = (labelBalance1.text ?? "") + sender.tag.description
            if let myVal = value.double {
                if sender.tag == 11 {
                    if (labelBalance1.text?.count ?? 0)! == 1 {
                        labelBalance1.text = "0"
                        self.isDecimal = false
                        self.countDecimal = 0
                    }else {
                        labelBalance1.text = labelBalance1.text?.dropLast().description
                        if(self.isDecimal){
                            countDecimal = countDecimal - 1
                        }
                    }
                    self.apiCallToConvertCurrency()
                } else if sender.tag == 10 {
                    labelBalance1.text = (labelBalance1.text ?? "") + "."
                    self.isDecimal = true
                    countDecimal = 0
                } else if ((Double(value)! > (Double(user?.bteBalance ?? "0")! - K_TRANSFER_FEES)) && showBTE) || ((Double(value ?? "0")! > Double(user?.currencyBalance ?? "0")!) && !showBTE) {
                    showAlert(title: "Error", message: "Entered value should not be greater than available balance", action1Name: "Ok", action2Name: nil)
                    return
                }else if ((Double((self.unclearedAmount.available_bte_aud ?? "0"))! - Double(beValue ?? "0")! - K_TRANSFER_FEES < 0) && self.switchLabel.text == "BTE-AUD") {
                    showAlert(title: "Error", message: "Entered value should not be greater than available balance", action1Name: "Ok", action2Name: nil)
                    return
                } else {
                    if labelBalance1.text == "0" || labelBalance1.text == "" {
                        labelBalance1.text = sender.tag.description
                    } else {
                        if(self.isDecimal){
                            if (countDecimal <= 1){
                                labelBalance1.text = (labelBalance1.text ?? "") +  sender.tag.description
                                countDecimal = countDecimal + 1
                            }else {
                                labelBalance1.text = (labelBalance1.text ?? "") +  ""
                            }
                        }else {
                            labelBalance1.text = (labelBalance1.text ?? "") + sender.tag.description
                            self.isDecimal = false
                            countDecimal = 0
                        }
                    }
                    self.apiCallToConvertCurrency()
                }
            }else {
                labelBalance1.text = "0"
            }
    
    }
    
    @IBAction func pasteAmount(_ sender: UIButton) {
        if let value = UIPasteboard.general.string, let validNumber = Double(value) {
            if ((validNumber > Double(user?.bteBalance ?? "0")!) && showBTE) || ((validNumber > Double(user?.currencyBalance ?? "0")!) && !showBTE) {
                showAlert(title: "Error", message: "Entered value should not be greater than available balance", action1Name: "Ok", action2Name: nil)
            } else {
                self.labelBalance1.text = validNumber.description
                self.apiCallToConvertCurrency()
            }
        } else {
            self.showAlert(title: "Error", message: "Please paste valid amount", action1Name: "Ok", action2Name: nil)
        }
    }
    
    @IBAction func transferAllBalance(_ sender: Any) {
        let bal = Double((user?.bteBalance) ?? "0")! - 20.0
        amount = Double(String(format: "%.2f", bal))!
        labelBalance1.text = String(format: "%.2f", bal)
        labelBalanceTypeFrom.text = "BTE"
        labelBalance2.text = user?.currencyBalance
        labelBalanceTypeTo.text = user?.currencyCode
        self.labelBalAfter.text = "BALANCE AFTER: 0 \(self.switchLabel.text ?? "")"
    }
    
    @IBAction func transfer(_ sender: UIButton) {
        if let myVal = labelBalance1.text?.double {
            if !(Double(labelBalance1.text ?? "0")! > 0) {
                self.showAlert(title: "Error", message: "Enter Amount", action1Name: "Ok", action2Name: nil)
            } else {
                showPopUp(view: finalPreviewPopUp, fundTransfer: nil)
            }
        }else {
            self.labelBalance1.text = "0"
            self.labelBalance2.text = "0"
        }
    }
    
    @IBAction func proceed(_ sender: UIButton) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePasscode), name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.presentPasscodeScreen), name: NSNotification.Name(N_PRESENT_PASSCODE), object: nil)
        passCodeScreen = "AmountScreen"
        Singleton.shared.authenticationWithTouchID(cancel: true, completionHandler: {
            DispatchQueue.main.async {
                self.hidePopUp(view: self.finalPreviewPopUp, popBack: false)
            }
            self.apiCallToTransfer()
        })
    }
    
    @IBAction func applyAction(_ sender: Any) {
        if(self.coupenField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Please enter coupon code", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "coupon_code":self.coupenField.text ?? "",
                "merchant": transferTo,
                "amount": self.amount,
                "asset": self.showBTE ? self.switchLabel.text == "BTE" ? "bte":"bteaud":self.switchValueBottom.text == "BTE" ? "bte":"bteaud",
                "gst_included":self.gstTick.image == nil ? 0:1,
            ]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_VALIDATE_REWARD, method: .post, parameter: param, objectClass: CoupenResponse.self, requestCode: U_VALIDATE_REWARD, userToken: nil) { response in
                self.counpenData = response
                if(self.counpenData.data.discount_amount != nil){
                    self.coupenNote.text = "Coupon code applied. You'll get cashback of $\(response.data.discount_amount ?? 0) AUD"
                }else {
                    self.coupenNote.text = response.message ?? ""
                }
                ActivityIndicator.hide()
            }
        }
    }
    
    @objc func handlePasscode() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
        DispatchQueue.main.async {
            self.hidePopUp(view: self.finalPreviewPopUp, popBack: false)
        }
        self.apiCallToTransfer()
    }
    
    @objc func presentPasscodeScreen() {
        passCodeScreen = "AmountScreen"
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_PRESENT_PASSCODE), object: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EnterPasscodeViewController") as! EnterPasscodeViewController
        self.navigationController?.pushViewController(myVC, animated: false)
    }
    
    @IBAction func closePopUp(_ sender: UIButton) {
        hidePopUp(view: transferConfirmPopUp, popBack: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gstAction(_ sender: Any) {
        if(gstTick.image == nil){
            self.gstTick.image = UIImage(named: "checkmark")
        }else {
            self.gstTick.image = nil
        }
        
        self.applyAction(self)
    }
    
    @IBAction func switchCurrency(_ sender: Any) {
        showBTE = !showBTE
        handleBalance(switchCurrency: true)
    }
    
    @IBAction func switchAction(_ sender: UIButton) {
        self.popupView.isHidden = false
    }
    
    @IBAction func selectCurrencyAction(_ sender: UIButton) {
        self.popupView.isHidden = true
        if(sender.tag == 3){
            return
        }
        if(sender.tag == 1){
            self.switchLabel.text = "BTE"
            self.switchValueBottom.text = "BTE"
        }else {
            self.switchLabel.text = "BTE-AUD"
            self.switchValueBottom.text = "BTE-AUD"
        }
        
        labelBalance1.text = "0"
        labelBalance2.text = "0"
        handleBalance(switchCurrency: false)
    }
}
