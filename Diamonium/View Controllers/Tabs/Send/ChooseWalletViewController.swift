//
//  ChooseWalletViewController.swift
//  Diamonium
//
//

import UIKit
import RealmSwift

class ChooseWalletViewController: UIViewController {
    var transferTo = String()
    var beValue: String?
    var localValue: String?
    var accountList: Results<DBUsers>!
    var isTransferBetween = false
    
    var selectedCoupen:String?
    var selecteBte:String?
    var selectedGst: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (transferTo.isEmpty) && transferTo != "" {
            accountList = DBManager.sharedInstance.getUsersFromDB().filter("token != '\(Singleton.shared.userToken)'")
        } else {
            accountList = DBManager.sharedInstance.getUsersFromDB().filter("accNumber != '\(transferTo)'")
        }
    }
    
    //MARK: Action
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension ChooseWalletViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! WalletTableViewCell
        
        let user = accountList[indexPath.row]
        cell.labelUsername.text = user.accNumber
        cell.labelAccType.text = (user.bteBalance ?? "") + " BTE"
        SessionManager.shared.methodForApiCalling(url: U_GET_CLEARED_AMOUNT + "\(user.accNumber ?? "")", method: .get, parameter: nil, objectClass: GetUnclearedAmount.self, requestCode: U_GET_CLEARED_AMOUNT, userToken: nil) { response in
            let data = self.getValueRemovingComma(response: response)
            cell.bteaudLabel.text = (data.available_bte_aud ?? "") + " BTE-AUD"
            ActivityIndicator.hide()
        }
        if let image = user.profileImage, image != "" {
            cell.profileImage.sd_setImage(with: URL(string: user.profileImage!)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        }
        return cell
    }
}
extension ChooseWalletViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = accountList[indexPath.row]
        
        let amountController = storyboard?.instantiateViewController(withIdentifier: "AmountViewController") as! AmountViewController
         if(isTransferBetween){
            amountController.user = DBManager.sharedInstance.currentUser[0]
            amountController.transferTo = user.accNumber
         }else if (transferTo.isEmpty){
            amountController.user = DBManager.sharedInstance.currentUser[0]
            amountController.transferTo = user.accNumber
         }else {
            amountController.user = user
            amountController.transferTo = transferTo
        }
        amountController.beValue = beValue
        amountController.localValue = localValue
        amountController.selectedCoupen = self.selectedCoupen
        amountController.selecteBte = self.selecteBte
        amountController.selectedGst = self.selectedGst
        navigationController?.pushViewController(amountController, animated: true)
    }
}
