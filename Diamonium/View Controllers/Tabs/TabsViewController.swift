//
//  TabViewController.swift
//  Diamonium
//
//

import UIKit
import FirebaseRemoteConfig
//import Crashlytics


class TabsViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var imageViewHome: UIImageView!
    @IBOutlet weak var buttonHome: UIButton!
    @IBOutlet weak var imageViewReceive: UIImageView!
    @IBOutlet weak var buttonReceive: UIButton!
    @IBOutlet weak var imageViewSend: UIImageView!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var imageViewSwap: UIImageView!
    @IBOutlet weak var buttonSwap: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var navBarHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var bottomBarHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var homeImage: UIImageView!
    @IBOutlet weak var homeButton: UIButton!
    
    
    var imageView: [UIImageView]?
    var button: [UIButton]?
    var currentIndex = 0
    
    //Remote Config
    var remoteConfig: RemoteConfig!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        fetchConfig()
        
        imageView = [imageViewSend, imageViewReceive, imageViewHome, imageViewSwap, homeImage]
        button = [buttonSend, buttonReceive, buttonHome, buttonSwap, homeButton]
        if(DBManager.sharedInstance.currentUser[0].accType == "Business"){
            self.labelTitle.text = "BARTEOS MERCHANT WALLET"
        }else{
            self.labelTitle.text = "BARTEOS AUSTRALIAN WALLET"
        }
        view.backgroundColor = barBackgroundColor
        navBarView.backgroundColor = barBackgroundColor
        tabBarView.backgroundColor = barBackgroundColor
        
        showSelectedTab(6)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        
//        setNavigationBar(false)
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "smallLogo"), style: .bordered, target: self, action: nil)
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings.png"), style: .bordered, target: self, action: #selector(settings))
        PageViewController.index_delegate = self
    }
    
//    func setTitle(index: Int) {
//        switch index {
//        case 0:
//            labelTitle.text = "Home"
//            break
//        case 1:
//            labelTitle.text = "Receive"
//            break
//        case 2:
//            labelTitle.text = "Send"
//            break
//        case 3:
//            labelTitle.text = "Settings"
//            break
//        default:
//            print("default")
//        }
//    }
    
    func showSelectedTab(_ selectedIndex: Int) {
//        setTitle(index: selectedIndex)
        for index in 0..<(imageView?.count)! {
            if index == selectedIndex {
                imageView?[index].image = imageView?[index].image?.withRenderingMode(.alwaysTemplate)
                imageView?[index].tintColor = primaryColor
                button?[index].setTitleColor(primaryColor, for: .normal)
            } else {
                imageView?[index].image = imageView?[index].image?.withRenderingMode(.alwaysTemplate)
                imageView?[index].tintColor = .black
                button?[index].setTitleColor(.black, for: .normal)
            }
        }
    }
    
    func fetchConfig() {
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
                self.display()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
            
        }
    }
    
    func display() {
        var softUpdate =  "update_soft_ios_bepay"
        var updateRequired = "force_update_required_ios_bepay"
        var currentVersion = "force_update_current_version_ios_bepay"
        var updateUrl = "force_update_store_url_ios_bepay"
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        let supportUrl = "force_update_support_url_bepay"
        if let supportUrl = remoteConfig["force_update_support_url_bepay"].stringValue as? String {
            U_CREATE_SUPPORT_TICKET = supportUrl
        }
        
        if(remoteConfig[updateRequired].stringValue == "true"){
            if(remoteConfig[currentVersion].stringValue == appVersion){
                return
            }else{
                if(remoteConfig[softUpdate].stringValue == "false"){
                    self.showPopup(title: "Update", msg: "New Version is available on App Store",action:2)
                }else{
                    self.showPopup(title: "Update", msg:  "New Version is available on App Store",action: 1)
                }
            }
        }else {
            return
        }
    }
    
    func showPopup(title: String, msg: String,action:Int?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Update", style: .default) { (UIAlertAction) in
            self.yesButtonAction()
        }
        let noBtn = UIAlertAction(title: "Cancel", style: .default){
            (UIAlertAction) in
            if(action != 1){
                self.forceUpdate()
            }
        }
        
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        
        present(alert, animated: true, completion: nil)
    }
    
    func yesButtonAction(){
        let string = remoteConfig["force_update_store_url_ios_bepay"].stringValue!
        let url  = NSURL(string: string)//itms   https
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.openURL(url! as URL)
        }
    }
    
    func forceUpdate() {
        exit(0);
    }
    
    //MARK: Action
    @IBAction func settings(_ sender: UIButton) {
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        //Crashlytics.sharedInstance().crash()
        self.navigationController?.pushViewController(settingVC, animated: true)
//        self.present(settingVC, animated: true, completion: nil)
    }
    
    @IBAction func home(_ sender: UIButton) {
        showSelectedTab(2)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerThird()
    }
    
    @IBAction func receive(_ sender: UIButton) {
        showSelectedTab(1)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerSecond()
    }
    
    @IBAction func scan(_ sender: UIButton) {
        let scanContoller = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
        self.present(scanContoller, animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: UIButton) {
        showSelectedTab(0)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerFirst()
    }
    
    @IBAction func swap(_ sender: UIButton) {
        showSelectedTab(3)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerFourth()
    }
    
    @IBAction func homeAction(_ sender: Any) {
        showSelectedTab(5)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerLast()
    }
    
}

extension TabsViewController: ControllerIndexDelegate {
    func getControllerIndex(index: Int) {
        if(index == 6 && (DBManager.sharedInstance.currentUser[0].accType == "Business")){
            showSelectedTab(6)
            let pageVC = PageViewController.dataSource1 as? PageViewController
            pageVC?.setControllerLast()
        }else {
            showSelectedTab(index)
        }
    }
}
