//
//  ReceiveViewController.swift
//  Diamonium
//
//

import UIKit

class ReceiveViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var imageQRCode: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var clipboard: View!
    @IBOutlet weak var textFieldBalance1: UITextField!
    @IBOutlet weak var labelBalanceTypeFrom: UILabel!
    @IBOutlet weak var labelBalance2: UILabel!
    @IBOutlet weak var labelBalanceTypeTo: UILabel!
//        @IBOutlet weak var textFieldBeValue: UITextField!
//    @IBOutlet weak var labelCurrencyName: UILabel!
//    @IBOutlet weak var textFieldCurrencyValue: UITextField!
    
    @IBOutlet weak var gstTick: ImageView!
    @IBOutlet weak var coupenField: UITextField!
    @IBOutlet weak var coupenNote: UILabel!
    
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var switchViewBottom: UIView!
    @IBOutlet weak var switchValueBottom: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var couponView: View!
    
    var user = DBManager.sharedInstance.currentUser[0]
//    var from: String?
//    var to: String?
    var showBTE: Bool = false
    var counpenData = CoupenResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.coupenField.delegate = self
        usernameView.layer.borderWidth = 1.0
        usernameView.layer.borderColor = primaryColor.cgColor
         textFieldBalance1.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: NSNotification.Name("updateUserData"), object: nil)
    
        textFieldBalance1.addDoneOnKeyboardWithTarget(self, action: #selector(done(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        user = DBManager.sharedInstance.currentUser[0]
        labelBalanceTypeFrom.text = user.currencyCode
        labelUsername.text = user.accNumber!
        self.coupenField.text = ""
        self.gstTick.image = nil
        self.coupenNote.text = ""
        if(user.accType == "Business" || user.accType == "Non-Profit"){
            self.couponView.isHidden = false
        }else {
            self.couponView.isHidden = true
        }
        
        generateQRCodeWithNameAmount(bte: "0", localValue: "0")
    }
    
    @objc func updateBalance() {
        textFieldBalance1.text = "0"
        labelBalance2.text = "0"
    }
    
    func generateQRCodeWithNameAmount(bte: String, localValue: String) {
        let isGst = self.gstTick.image == UIImage(named: "checkmark") ? "1":"0"
        if(self.coupenNote.isHidden){
            let qrValue = (user.accNumber!) + "+" + bte + "+" + localValue + "+" + (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: "") + "+" + isGst + "+"
            imageQRCode.image = generateQRCode(from: qrValue)
        }else {
            let qrValue = (user.accNumber!) + "+" + bte + "+" + localValue + "+" + (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: "") + "+" + isGst + "+" + (self.coupenField.text ?? "")
            imageQRCode.image = generateQRCode(from: qrValue)
        }
       
    }
    
    @objc func done(_ : UIBarButtonItem) {
        textFieldBalance1.resignFirstResponder()
        getAmount(text: textFieldBalance1.text)
    }
    
    func apiCallToConvertCurrency(_ amount: Double) {
        if amount > 0 {
            var param = [String:Any]()
            if(showBTE){
                param = [K_AMOUNT: amount,
                  K_CURRENCY_FROM: (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: ""),
                    K_CURRENCY_TO: labelBalanceTypeTo.text!]
            }else {
               param = [K_AMOUNT: amount,
                             K_CURRENCY_FROM: labelBalanceTypeFrom.text ?? "",
                   K_CURRENCY_TO: (self.switchLabel.text ?? "").replacingOccurrences(of: "-", with: "")
               ]
            }
        
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
                
                if !(self.showBTE) {
                    self.labelBalance2.text = String(format: "%.2f", (balance.be_value) ?? 0)
                } else {
                    self.labelBalance2.text = String(format: "%.2f", (balance.local_value) ?? 0)
                }
                
                self.generateQRCodeWithNameAmount(bte: String(format: "%.2f", balance.be_value ?? 0), localValue: String(format: "%.2f", self.showBTE ? (balance.local_value ?? 0):(amount ?? 0)))
                ActivityIndicator.hide()
            }
        } else {
            textFieldBalance1.text = 0.description
            self.labelBalance2.text = 0.description
            self.generateQRCodeWithNameAmount(bte: "0", localValue: "0")
        }
    }
    
    func getAmount(text: String?) {
        if let value = text, let validNumber = Double(value) {
            apiCallToConvertCurrency(Double(text ?? "0")!)
        } else {
            textFieldBalance1.text = 0.description
            labelBalance2.text = 0.description
            self.generateQRCodeWithNameAmount(bte: "0", localValue: "0")
        }
    }
    
//    func generateQRCode(from string: String) -> UIImage? {
//        let data = string.data(using: String.Encoding.utf8)
//        if let filter = CIFilter(name: "CIQRCodeGenerator") {
//            guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
//
//            filter.setValue(data, forKey: "inputMessage")
//
////            filter.setValue("H", forKey: "inputCorrectionLevel")
//            colorFilter.setValue(filter.outputImage, forKey: "inputImage")
//            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1") // Background white
//            colorFilter.setValue(CIColor(red: 229/255, green: 178/255, blue: 69/255), forKey: "inputColor0") // Foreground or the barcode RED
//            guard let qrCodeImage = colorFilter.outputImage
//                else {
//                    return nil
//            }
//            let scaleX = imageQRCode.frame.size.width / qrCodeImage.extent.size.width
//            let scaleY = imageQRCode.frame.size.height / qrCodeImage.extent.size.height
//            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
//
//            if let output = colorFilter.outputImage?.transformed(by: transform) {
//                return UIImage(ciImage: output)
//            }
//        }
//        return nil
//    }
    
    //MARK: Action
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: labelUsername.text)
    }
    
    @IBAction func pasteAmount(_ sender: UIButton) {
        if let value = UIPasteboard.general.string, let validNumber = Double(value) {
            self.textFieldBalance1.text = validNumber.description
            self.apiCallToConvertCurrency(validNumber)
        } else {
            self.showAlert(title: "Error", message: "Please paste valid amount", action1Name: "Ok", action2Name: nil)
        }
    }
    
    @IBAction func switchCurrency(_ sender: Any) {
        showBTE = !showBTE
        textFieldBalance1.text = "0"
        if showBTE {
            labelBalanceTypeFrom.text = "BTE"
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = user.currencyCode
            
            self.labelBalanceTypeFrom.isHidden = true
            self.switchView.isHidden = false
            self.switchViewBottom.isHidden = true
            self.switchLabel.text = "BTE"
            self.switchValueBottom.text = "BTE"
            self.labelBalanceTypeTo.isHidden = false
        } else {
            labelBalanceTypeFrom.text = user.currencyCode
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = "BTE"
            
            self.labelBalanceTypeFrom.isHidden = false
            self.switchView.isHidden = true
            self.switchViewBottom.isHidden = false
            self.switchLabel.text = "BTE"
            self.switchValueBottom.text = "BTE"
            self.labelBalanceTypeTo.isHidden = true
        }
        generateQRCodeWithNameAmount(bte: "0", localValue: "0")
    }
    
    @IBAction func switchAction(_ sender: UIButton) {
        self.popupView.isHidden = false
    }
    
    
    @IBAction func selectCurrencyAction(_ sender: UIButton) {
        self.popupView.isHidden = true
        if(sender.tag == 3){
            return
        }
        if(sender.tag == 1){
            self.switchLabel.text = "BTE"
            self.switchValueBottom.text = "BTE"
        }else {
            self.switchLabel.text = "BTE-AUD"
            self.switchValueBottom.text = "BTE-AUD"
        }
        textFieldBalance1.text = "0"
        labelBalance2.text = "0"
    }
    
    @IBAction func applyCodeAction(_ sender: UIButton) {
        if(self.coupenField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Please enter coupon code", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "coupon_code":self.coupenField.text ?? "",
                "amount": self.showBTE ?  self.textFieldBalance1.text ?? "":self.labelBalance2.text ?? "",
                "merchant":DBManager.sharedInstance.currentUser[0].accNumber ?? "",
                "asset": self.showBTE ? self.switchLabel.text == "BTE" ? "bte":"bteaud":self.switchValueBottom.text == "BTE" ? "bte":"bteaud",
                "gst_included":self.gstTick.image == nil ? 0:1
            ]
            self.textFieldBalance1.resignFirstResponder()
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_VALIDATE_REWARD, method: .post, parameter: param, objectClass: CoupenResponse.self, requestCode: U_VALIDATE_REWARD, userToken: nil) { response in
                self.counpenData = response
                if(self.counpenData.data.discount_amount != nil){
                    self.coupenNote.text = "Coupon code applied. You'll get cashback of $\(response.data.discount_amount ?? 0) AUD"
                    self.apiCallToConvertCurrency(Double(self.textFieldBalance1.text ?? "0")!)
                }else {
                    self.coupenNote.text = response.message ?? ""
                   
                }
                ActivityIndicator.hide()
            }
        }
    }
    
    @IBAction func gstAction(_ sender: UIButton) {
        if(gstTick.image == nil){
            self.gstTick.image = UIImage(named: "checkmark")
        }else {
            self.gstTick.image = nil
        }
        self.applyCodeAction(sender)
    }
}
extension ReceiveViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField != self.coupenField){
            self.coupenField.text = ""
            self.coupenNote.text = ""
        if (textField.text?.count == 1) && string == "" {
            getAmount(text: "0")
        }else if string == "" {
            getAmount(text: textField.text?.dropLast().description)
        }else {
            getAmount(text: textField.text!+string)
        }
        }else if(textField == self.coupenField){
            self.coupenNote.text = ""
            self.apiCallToConvertCurrency(Double(self.textFieldBalance1.text ?? "0")!)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.textFieldBalance1){
            self.textFieldBalance1.text = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == self.coupenField){
            if(textField.text == ""){
                self.coupenNote.text = ""
            }
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
