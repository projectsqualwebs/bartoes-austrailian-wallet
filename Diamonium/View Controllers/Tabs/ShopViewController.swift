//
//  ShopViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 29/07/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit
import WebKit

class ShopViewController: UIViewController,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.hide()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ActivityIndicator.hide()
    }
    //MARK: IBOUtlets
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var url = NSURL(string:"https://www.beshop.com.au")
        var req = NSURLRequest(url:url as! URL)
        self.webView.load(req as URLRequest)
        ActivityIndicator.show(view: self.view)
        self.webView.navigationDelegate  = self
    }
}
