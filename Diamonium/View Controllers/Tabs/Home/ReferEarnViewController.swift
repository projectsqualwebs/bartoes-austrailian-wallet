//
//  ReferEarnViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 15/10/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit

class ReferEarnViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var referralCode: UITextField!
    @IBOutlet weak var rewardLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rewardLabel.text = "Earn $" + K_REFER_AMOUNT
        self.referralCode.text = "https://www.beshop.com.au/download Hey checkount this app. Use my referral code: \(DBManager.sharedInstance.currentUser[0].accNumber ?? "")"
    }
    
    //MARK: IBActions
    @IBAction func copyAction(_ sender: Any) {
        UIPasteboard.general.string = self.referralCode.text ?? ""
    }
    
    @IBAction func referAction(_ sender: Any) {
      
        // Setting description
        let firstActivityItem = "https://www.beshop.com.au/download Hey checkount this app. Use my referral code: \(DBManager.sharedInstance.currentUser[0].accNumber ?? "")"

           // Setting url
           let secondActivityItem : NSURL = NSURL(string: "https://www.beshop.com.au/download")!
           
           // If you want to use an image
           let image : UIImage = UIImage(named: "reward-1")!
           let activityViewController : UIActivityViewController = UIActivityViewController(
               activityItems: [firstActivityItem, secondActivityItem, image], applicationActivities: nil)
           
           // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.view
           
           // This line remove the arrow of the popover to show in iPad
           activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
           activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
           
           
           
           // Anything you want to exclude
           activityViewController.excludedActivityTypes = [
               UIActivity.ActivityType.postToWeibo,
               UIActivity.ActivityType.print,
               UIActivity.ActivityType.assignToContact,
               UIActivity.ActivityType.saveToCameraRoll,
               UIActivity.ActivityType.addToReadingList,
               UIActivity.ActivityType.postToFlickr,
               UIActivity.ActivityType.postToVimeo,
               UIActivity.ActivityType.postToTencentWeibo,
               UIActivity.ActivityType.postToFacebook
           ]
            self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
