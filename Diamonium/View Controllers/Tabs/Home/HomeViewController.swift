//
//  HomeViewController.swift
//  Diamonium
//
//

import UIKit
import RealmSwift
import Crashlytics
import Charts


class HomeViewController: UIViewController, ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {

            guard let dataSet = chartView.data?.dataSets[highlight.dataSetIndex] else { return }
            let entryIndex = dataSet.entryIndex(entry: entry)
        
        markerViewLabel.font = UIFont.systemFont(ofSize: 10)
        let a = self.chartData.bteFeed[entryIndex]
        let time = self.convertTimestampToDate(Int(a[0]), to: "dd MMM")
        let value = Double(round(100 * a[1]) / 100)
        markerViewLabel.text = "\(value), \(time)"
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var labelAccountType: UILabel!
    @IBOutlet weak var labelAccountNumber: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelDiamBalance: UILabel!
    @IBOutlet weak var diamBalanceDecimal: UILabel!
    @IBOutlet weak var labelTokenBalance: UILabel!
    @IBOutlet weak var labelBalanceAccToCurrency: UILabel!
    @IBOutlet weak var tokenBalanceDecimal: UILabel!
    @IBOutlet weak var chartView: LineChartView!
    
    @IBOutlet weak var btePrice: UILabel!
    @IBOutlet weak var businessCount: UILabel!
    @IBOutlet weak var consumerCount: UILabel!
    @IBOutlet weak var circulationLabel: UILabel!
    @IBOutlet weak var rewardValue: UILabel!
    @IBOutlet weak var tokenBalanceAccToCurrency: UILabel!
    @IBOutlet weak var swapButton: CustomButton!
    
    
    var arrayNewsFeed = [Feeds]()
    let formatter = NumberFormatter()
    var userToken: String?
    var user: DBUsers?
    var chartData = BTEFeeds()
    var currentSlide = 0
    var currentAvailable = "0"
    var currentTotal = "0"
    var markerView = MarkerView(frame: CGRect(x: 0, y: 0, width: 150, height: 20))
    let markerViewLabel = UILabel(frame: CGRect(x: 0, y: 0, width:120, height: 20))
    
    
    let closedFeeds = DBManager.sharedInstance.getClosedFeedsFromDB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        markerView.addSubview(markerViewLabel)
        userToken = user?.token
        formatter.groupingSize = 3
        formatter.secondaryGroupingSize = 3
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        let circulation = DBManager.sharedInstance.getCircluationFromDB()
        Singleton.shared.unclearedAmount =  GetUnclearedAmount(total_bte: circulation.total_bte, available_bte: circulation.available_bte, bte_aud: circulation.bte_aud, total_aud: circulation.total_aud, available_bte_aud: circulation.available_bte_aud, available_aud: circulation.available_aud)
       // self.changeCurrentBalance(amount: circulation.total_aud ?? "", currentBalance: circulation.available_aud ?? "")
        self.changeCurrentBalance(amount: circulation.bte_aud ?? "", currentBalance: circulation.available_bte_aud ?? "")
        
        if let feeds = UserDefaults.standard.value(forKey: UD_BTE_FEED){
            let decoder = JSONDecoder()
            if let data = try? decoder.decode(BTEFeeds.self, from: feeds as! Data){
                self.chartData = BTEFeeds(bteFeed: data.bteFeed ?? [[]], user_feed: data.user_feed ?? [[]], shop: data.shop ?? [[]])
            }
        }
        let bteLasts = DBManager.sharedInstance.getBTELastFeedFromDB()
        let bte = Double(bteLasts.bte ?? "0")!
        self.btePrice.text = "$\(Double(round(100*bte)/100))"
        self.businessCount.text = bteLasts.no_shops
        self.consumerCount.text = bteLasts.users
        
        if let totalCirc = UserDefaults.standard.value(forKey: UD_TOTAL_CIRCULATION) as? String {
            self.circulationLabel.text = "Circ: \(totalCirc ?? "")"
        }
        
        self.initializeChartView()
        
        updateUserData()
        
        //updateBalance()
        
        self.labelTokenBalance.numberOfLines = 1
        self.labelTokenBalance.adjustsFontSizeToFitWidth = true
        self.labelDiamBalance.numberOfLines = 1
        self.labelDiamBalance.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(apiCallToGetDiamBalance), name: NSNotification.Name("updateCurrency"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: NSNotification.Name("updateBalance"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserData), name: NSNotification.Name("updateUserData"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let info = UserDefaults.standard.value(forKey: "notificationInfo") {
            UserDefaults.standard.removeObject(forKey: "notificationInfo")
            // UserDefaults.standard.set("Show loader", forKey: "transUpdate")
            // self.getCurrentBalance()
            self.apiCallToGetDiamBalance()
            
            print(info)
        }else {
            print("ok")
            
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_REWARD_VALUE, method: .get, parameter: nil, objectClass: [Response].self, requestCode: U_GET_REWARD_VALUE, userToken: nil) { response in
            self.rewardValue.text = "Earn $\(response[0].refer_amount ?? 0)"
            K_REFER_AMOUNT = "\(response[0].refer_amount ?? 0)"
        }
    }
    
    func initializeChartView(){
        
        chartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries = [LineChartDataSet]()
        
        let yVals1 = (0..<self.chartData.bteFeed.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: self.chartData.bteFeed[i][0], y: self.chartData.bteFeed[i][1])
        }
        
        let yVals2 = (0..<self.chartData.user_feed.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: self.chartData.user_feed[i][0], y: self.chartData.user_feed[i][1])
        }
        let yVals3 = (0..<self.chartData.shop.count).map { (i) -> ChartDataEntry in
            return ChartDataEntry(x: self.chartData.shop[i][0], y: self.chartData.shop[i][1])
        }
        
        self.chartView.layer.cornerRadius = 15
        
        let set1 = LineChartDataSet(entries: yVals1, label: "BTE FEED")
        set1.axisDependency = .left
        set1.setColor(UIColor(red: 20/255, green: 255/255, blue: 50/255, alpha: 1))
        set1.lineWidth = 1
        set1.circleRadius = 0
        set1.fillAlpha = 40/255
        set1.drawValuesEnabled = false
      //  set1.fillColor = .clear//UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)
        set1.highlightColor = UIColor(red: 20/255, green: 255/255, blue: 50/255, alpha: 1)
        set1.highlightEnabled = true
        set1.drawCircleHoleEnabled = true
        set1.circleRadius = 6
        set1.circleHoleColor = goldenColor
       // set1.setCircleColor(goldenColor)
        set1.fill = Fill.fillWithColor(lightGreen.withAlphaComponent(0.8))
        set1.drawFilledEnabled = false
        
        
        
    
    
    
        set1.setCircleColor(.clear)
        set1.lineWidth = 2
        set1.circleRadius = 0
        set1.fillAlpha = 0/255
        set1.drawValuesEnabled = false
        set1.fillColor = .clear//UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)
        set1.highlightColor = UIColor(red: 20/255, green: 255/255, blue: 50/255, alpha: 1)
        set1.drawCircleHoleEnabled = false
        
        
        
        let set2 = LineChartDataSet(entries: yVals2, label: "USER FEED")
        set2.axisDependency = .right
        set2.setColor(UIColor(red: 100/255, green: 255/255, blue: 150/255, alpha: 1))
        set2.setCircleColor(.clear)
        set2.lineWidth = 2
        set2.circleRadius = 0
        set2.fillAlpha = 0/255
        set2.fillColor = .clear
        set2.drawValuesEnabled = false
        set2.highlightColor = UIColor(red: 100/255, green: 255/255, blue: 150/255, alpha: 1)
        set2.drawCircleHoleEnabled = false
        
        let set3 = LineChartDataSet(entries: yVals3, label: "SHOP")
        set3.axisDependency = .right
        set3.setColor(primaryColor)
        set3.setCircleColor(.clear)
        set3.lineWidth = 2
        set3.circleRadius = 0
        set3.fillAlpha = 0/255
        set3.drawValuesEnabled = false
        set3.fillColor = .clear//UIColor.yellow.withAlphaComponent(200/255)
        set3.highlightColor = primaryColor
        set3.drawCircleHoleEnabled = false
    
        
        let data = LineChartData(dataSets: [set1])
        //        data.addDataSet(set1)
        //        data.addDataSet(set2)
        //        data.addDataSet(set3)
        data.setValueTextColor(.white)
        data.setValueFont(.systemFont(ofSize: 9))
        
        chartView.data = data
        let xAxisValue = chartView.xAxis
        xAxisValue.valueFormatter = XAxisNameFormater()
        
        //Animation bars
        
        
        // X axis configurations
        chartView.xAxis.spaceMin = 0.5
        chartView.xAxis.spaceMax = 0.5
        chartView.xAxis.granularity = 1
        chartView.xAxis.granularityEnabled = true
        chartView.xAxis.drawAxisLineEnabled = true
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.labelFont = UIFont.systemFont(ofSize: 12.0)
        chartView.xAxis.labelTextColor = UIColor.black
        chartView.xAxis.labelPosition = .bottom
        chartView.setExtraOffsets(left: 10, top: 10, right: 10, bottom: 20)
       // chartView.leftAxis.axisMinimum = 0//max(-5.0, chartView.data!.yMin - 3.0)
       // chartView.leftAxis.axisMaximum = chartView.data!.yMax + 0.9 //min(10.0, chartView.data!.yMax + 1.0)
       // let count = Int(chartView.leftAxis.axisMaximum - chartView.leftAxis.axisMinimum)
        
       // if(count != 0){
            chartView.leftAxis.labelCount = 10
       // }
        chartView.leftAxis.drawZeroLineEnabled = false
        chartView.notifyDataSetChanged()
        
        
        // Right axis configurations
        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        
        chartView.leftAxis.drawLabelsEnabled = true
        chartView.leftAxis.drawAxisLineEnabled = true
        chartView.leftAxis.drawGridLinesEnabled = false
        
        // Other configurations
        chartView.scaleXEnabled = true
        chartView.scaleYEnabled = true
        chartView.backgroundColor = lightGreen
        chartView.layer.cornerRadius = 10
        chartView.legend.enabled = false
        chartView.delegate = self
        chartView.marker =  markerView
    }
    
    func changeCurrentBalance(amount: String, currentBalance: String){
        if(amount == "" || currentBalance == ""){
            return
        }else if(amount.lowercased() == "nan" || currentBalance.lowercased() == "nan"){
            return
        }
        
//        if(self.currentSlide == 0){
//            labelBalanceAccToCurrency.text = "TOTAL BALANCE AUD"
//            let balanceAmount = Double(amount == "" ? "0":amount.replacingOccurrences(of: ",", with: ""))!
//            let numberFormatter = NumberFormatter()
//            numberFormatter.numberStyle = .decimal
//            let formattedNumber = numberFormatter.string(from: NSNumber(value:Int(balanceAmount)))
//            labelDiamBalance.text = "$\(formattedNumber ?? "")"
//            let decimal = Double(round(100*(balanceAmount - Double(Int(balanceAmount))))/100)
//
//            self.diamBalanceDecimal.text = "\(self.appendString(data: decimal))"
//            self.diamBalanceDecimal.text =    self.diamBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
//
//            tokenBalanceAccToCurrency.text = "TOTAL AVAILABLE AUD"
//            guard let tokenAmount = Double(currentBalance == "" ? "0":currentBalance.replacingOccurrences(of: ",", with: "")) else { return }
//            let decimal2 = Double(round(100*(tokenAmount - Double(Int(tokenAmount))))/100)
//            numberFormatter.numberStyle = .decimal
//            let formattedNumber2 = numberFormatter.string(from: NSNumber(value:Int(tokenAmount)))
//            labelTokenBalance.text = "$\(formattedNumber2 ?? "")"
//            self.tokenBalanceDecimal.text = "\(self.appendString(data: decimal2))"
//            self.tokenBalanceDecimal.text =    self.tokenBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
//
//        }else
        if(currentSlide == 1){
        
            labelBalanceAccToCurrency.text = "BALANCE BTE"
            let balanceAmount = Double(amount == "" ? "0":amount.replacingOccurrences(of: ",", with: ""))!
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:Int(balanceAmount)))
            labelDiamBalance.text = "\(formattedNumber ?? "")"
            let decimal = Double(round(100*(balanceAmount - Double(Int(balanceAmount))))/100)
            self.diamBalanceDecimal.text = "\(self.appendString(data: decimal))"
            self.diamBalanceDecimal.text =    self.diamBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
            
            tokenBalanceAccToCurrency.text = "AVAILABLE BTE"
            guard let tokenAmount = Double(currentBalance == "" ? "0":currentBalance.replacingOccurrences(of: ",", with: "")) else {
                return
            }
            let decimal2 = Double(round(100*(tokenAmount - Double(Int(tokenAmount))))/100)
            numberFormatter.numberStyle = .decimal
            let formattedNumber2 = numberFormatter.string(from: NSNumber(value:Int(tokenAmount)))
            labelTokenBalance.text = "\(formattedNumber2 ?? "" )"
            self.tokenBalanceDecimal.text = "\(self.appendString(data: decimal2))"
            self.tokenBalanceDecimal.text =    self.tokenBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
            
        }else if(currentSlide == 2){
            
            labelBalanceAccToCurrency.text = "BALANCE BTE-AUD"
            let balanceAmount = Double(amount == "" ? "0":amount.replacingOccurrences(of: ",", with: ""))!
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:Int(balanceAmount)))
            labelDiamBalance.text = "\(formattedNumber ?? "")"
            let decimal = Double(round(100*(balanceAmount - Double(Int(balanceAmount))))/100)
            self.diamBalanceDecimal.text = "\(self.appendString(data: decimal))"
            self.diamBalanceDecimal.text =    self.diamBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
            
            tokenBalanceAccToCurrency.text = "AVAILABLE BTE-AUD"
            guard let tokenAmount = Double(currentBalance == "" ? "0":currentBalance.replacingOccurrences(of: ",", with: "")) else {
                return
            }
            let decimal2 = Double(round(100*(tokenAmount - Double(Int(tokenAmount))))/100)
            numberFormatter.numberStyle = .decimal
            let formattedNumber2 = numberFormatter.string(from: NSNumber(value:Int(tokenAmount)))
            labelTokenBalance.text = "\(formattedNumber2 ?? "" )"
            self.tokenBalanceDecimal.text = "\(self.appendString(data: decimal2))"
            self.tokenBalanceDecimal.text =    self.tokenBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
        }else if(currentSlide == 3){
            labelBalanceAccToCurrency.text = "BALANCE BTE (\(Singleton.shared.selectedCurrency.code) VALUE)"
            let balanceAmount = Double(amount == "" ? "0":amount.replacingOccurrences(of: ",", with: ""))!
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:Int(balanceAmount)))
            labelDiamBalance.text = "\(formattedNumber ?? "")"
            let decimal = Double(round(100*(balanceAmount - Double(Int(balanceAmount))))/100)
            self.diamBalanceDecimal.text = "\(self.appendString(data: decimal))"
            self.diamBalanceDecimal.text =    self.diamBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
            tokenBalanceAccToCurrency.text = "AVAILABLE BTE (\(Singleton.shared.selectedCurrency.code) VALUE)"
            guard let tokenAmount = Double(currentBalance == "" ? "0":currentBalance.replacingOccurrences(of: ",", with: "")) else {
                return
            }
            let decimal2 = Double(round(100*(tokenAmount - Double(Int(tokenAmount))))/100)
            numberFormatter.numberStyle = .decimal
            let formattedNumber2 = numberFormatter.string(from: NSNumber(value:Int(tokenAmount)))
            labelTokenBalance.text = "\(formattedNumber2 ?? "" )"
            self.tokenBalanceDecimal.text = "\(self.appendString(data: decimal2))"
            self.tokenBalanceDecimal.text =    self.tokenBalanceDecimal.text!.replacingOccurrences(of: "0.", with: ".")
        }
    }
    
    @objc func updateBalance() {
        self.currentSlide = 2
        self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.bte_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte_aud ?? "")
        // self.currentSlide = 0
        // self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_aud ?? "")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            self.getCurrentBalance()
            self.getBteFeedData()
            self.getBteLastData()
            self.getCirculationCoin()
           // self.getCurrencyAmount()
        }
        
    }
    
    @objc func updateUserData() {
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerLast()
        PageViewController.index_delegate?.getControllerIndex(index: 4)
        if(DBManager.sharedInstance.currentUser.count > 0){
            labelUsername.text = DBManager.sharedInstance.currentUser[0].name ?? "Not Available"
            labelAccountType.text = DBManager.sharedInstance.currentUser[0].accType ?? "Personal"
            if(labelAccountType.text == "Individual"){
                self.labelAccountType.text = "Personal"
            }
            if(labelAccountType.text == "Individual"){
                self.labelAccountType.text = "Personal"
            }
            labelAccountNumber.text = DBManager.sharedInstance.currentUser[0].accNumber
            if let image = DBManager.sharedInstance.currentUser[0].profileImage, image != "" {
                userImage.sd_setImage(with: URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
            } else {
                userImage.image = #imageLiteral(resourceName: "defaultProfile.png")
            }
        }
        self.updateBalance()
        self.apiCallToGetDiamBalance()
        
        
        
        // apiCallToGetNewsFeeds()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            //  self.getCurrentBalance()
            self.updateLastTransaction()
        })
    }
    
    func updateLastTransaction() {
        if let transactionId = DBManager.sharedInstance.getLastTransactionId(){
            if(transactionId != nil && transactionId != ""){
                Router.getTransactionHistory(transId: transactionId)
            }
        }
    }

    @objc func apiCallToGetDiamBalance() {
        // labelBalanceAccToCurrency.text = "Balance \(Singleton.shared.selectedCurrency.code)"
        // self.labelDiamBalance.text = 0.description
        SessionManager.shared.apiCallToGetBalance { (balance, tokenBalance, beReward)  in
            self.currentAvailable = balance
            DispatchQueue.main.async {
                DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance, beRewards: beReward)
            }
        }
    }
    
    func apiCallToGetNewsFeeds() {
        SessionManager.shared.methodForApiCalling(url: U_BASE+U_GET_FEEDS, method: .get, parameter: nil, objectClass: FeedsResponse.self, requestCode: U_GET_FEEDS, userToken:nil) { (response) in
            self.arrayNewsFeed = [Feeds]()
            for index in 0..<(response.feeds?.count)! {
                let newsFeed = response.feeds?[index]
                var valueExist = false
                if (self.closedFeeds.count > 0) {
                    for feedIndex in self.closedFeeds {
                        if(newsFeed?.id?.description == feedIndex.id){
                            valueExist = true
                        }
                    }
                    if(!valueExist){
                        self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
                        valueExist = false
                    }
                }else {
                    self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
                }
            }
            
            ActivityIndicator.hide()
        }
    }
    
    func attributedString(_ value: String) -> NSMutableAttributedString {
        let attrValue = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let attrValue2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25)]
        let newValue = value.components(separatedBy: ".")
        let beforeDecimal = newValue[0]
        let afterDecimal = newValue[1]
        var newSTring = NSMutableAttributedString(string: "\(beforeDecimal).", attributes: attrValue2)
        newSTring.append(NSAttributedString(string: afterDecimal, attributes: attrValue))
        return newSTring
    }
    
    func handleSwapButton(bool: Bool){
        if(self.labelAccountType.text == "Business" || self.labelAccountType.text == "Non-Profit"){
            self.swapButton.isHidden = bool
        }else {
            self.swapButton.isHidden = true
        }
    }
    
    func apiCallToConvertCurrency(_ amount: String,type: Int) {
        if(Double(amount.replacingOccurrences(of: ",", with: "")) != 0){
        ActivityIndicator.show(view: self.view)
            var param = [String:Any]()
                param = [K_AMOUNT: Double(amount.replacingOccurrences(of: ",", with: "")),
                  K_CURRENCY_FROM: "BTE",
                    K_CURRENCY_TO: Singleton.shared.selectedCurrency.code]
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
                self.currentTotal = "\(balance.local_value ?? 0)"
                ActivityIndicator.hide()
            }
        }
    }
    
    //MARK: Action
    @IBAction func showProfile(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        controller.updateProfileDelegate = self
        self.puchController(controller: controller)
    }
    
    @IBAction func buyDiam(_ sender: UIButton) {
        let buyController = self.storyboard?.instantiateViewController(withIdentifier: "BuyDiamViewController") as! BuyDiamViewController
        navigationController?.pushViewController(buyController, animated: true)
    }

    
    @IBAction func changeClearedAmount(_ sender: Any) {
//        self.currentSlide = 0
//        self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_aud ?? "")
        self.currentSlide = 2
        self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.bte_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte_aud ?? "")
   
    }
    
    @IBAction func swapTokenAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SwapViewController") as! SwapViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settingAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func slideAction(_ sender: UIButton) {
        if(sender.tag == 1){
//            if(self.currentSlide == 0){
//                self.handleSwapButton(bool: true)
//                self.currentSlide = 3
//                self.changeCurrentBalance(amount: self.currentTotal, currentBalance: self.currentAvailable)
//            }else
            if(self.currentSlide == 2){
                self.handleSwapButton(bool: false)
                self.currentSlide = 1
                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_bte ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte ?? "")
            }else if(self.currentSlide == 1){
                self.handleSwapButton(bool: true)
                self.currentSlide = 3
                self.changeCurrentBalance(amount: self.currentTotal, currentBalance: self.currentAvailable)
 //       self.handleSwapButton(bool: false)
//                self.currentSlide = 0
//                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_aud ?? "")
            }else if(self.currentSlide == 3){
                self.handleSwapButton(bool: true)
                self.currentSlide = 2
                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.bte_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte_aud ?? "")
                
            }
        }else if(sender.tag == 2){
//            if(self.currentSlide == 0){
//                self.handleSwapButton(bool: true)
//                self.currentSlide = 1
//                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_bte ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte ?? "")
//            }else
            if(self.currentSlide == 1){
                self.handleSwapButton(bool: false)
                self.currentSlide = 2
                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.bte_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte_aud ?? "")
            }else if(self.currentSlide == 2){
                self.handleSwapButton(bool: false)
                self.currentSlide = 3
                self.changeCurrentBalance(amount: self.currentTotal, currentBalance: self.currentAvailable)
            }else if(self.currentSlide == 3){
                self.handleSwapButton(bool: true)
                self.currentSlide = 1
                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_bte ?? "", currentBalance: Singleton.shared.unclearedAmount.available_bte ?? "")
//                self.handleSwapButton(bool: true)
//                self.currentSlide = 0
//                self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_aud ?? "")
            }
        }
        if(currentSlide == 0){
            self.handleSwapButton(bool: true)
            self.changeCurrentBalance(amount: Singleton.shared.unclearedAmount.total_aud ?? "", currentBalance: Singleton.shared.unclearedAmount.available_aud ?? "")
        }
    }
    
    @IBAction func referAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferEarnViewController") as! ReferEarnViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension HomeViewController: UpdateProfileDelegate {
    func updateProfile(name: String?, profileImage: UIImage?) {
        userImage.image = profileImage
        labelUsername.text = name
    }
}



extension HomeViewController: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return self.convertTimestampToDate(Int(value), to: "MMM, dd")
    }
    
}

final class XAxisNameFormater: NSObject, IAxisValueFormatter {
    func stringForValue( _ value: Double, axis _: AxisBase?) -> String {
        var myVal = Int()
        var intValue:Int64 = 10000000000
        if(Int(value)/Int(truncatingIfNeeded: intValue) == 0){
            myVal = Int(value)
        }else {
            myVal = Int(value)/1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(myVal))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd\nMMM\nYY"
        return dateFormatter.string(from: date)
    }
    
}

extension HomeViewController {
    func getCurrentBalance(){
        if(DBManager.sharedInstance.currentUser.count > 0){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_GET_CLEARED_AMOUNT + "\(DBManager.sharedInstance.currentUser[0].accNumber ?? "")", method: .get, parameter: nil, objectClass: GetUnclearedAmount.self, requestCode: U_GET_CLEARED_AMOUNT, userToken: nil) { response in
                let data = self.getValueRemovingComma(response: response)
                Singleton.shared.unclearedAmount = data
                DBManager.sharedInstance.addCirculationAmount(total_bte: data.total_bte, available_bte: data.available_bte, bte_aud: data.bte_aud, total_aud: data.total_aud, available_bte_aud: data.available_bte_aud, available_aud: data.available_aud, be_rewards: data.be_rewards)
//                self.currentSlide = 0
//                self.handleSwapButton(bool: true)
//                self.changeCurrentBalance(amount: data.total_aud ?? "", currentBalance: data.available_aud ?? "")
                self.currentSlide = 2
                self.changeCurrentBalance(amount: data.bte_aud ?? "", currentBalance: data.available_bte_aud ?? "")
                self.apiCallToConvertCurrency(data.total_bte ?? "0", type: 1)
                ActivityIndicator.hide()
            }
        }
    }
    
    func getBteFeedData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_GET_BTE_FEEDS, method: .get, parameter: nil, objectClass: BTEFeeds.self, requestCode: U_GET_BTE_FEEDS, userToken: nil) { response in
            self.chartData = response
            let encoder = JSONEncoder()
            let data = try? encoder.encode(response)
            UserDefaults.standard.set(data, forKey: UD_BTE_FEED)
            self.initializeChartView()
            ActivityIndicator.hide()
        }
    }
    
    func getBteLastData(){
        if(Singleton.shared.bteFeedLastData.bte != nil){
            guard let value = Singleton.shared.bteFeedLastData.bte?.replacingOccurrences(of: ",", with: "") else {return}
            let bte = Double(value ?? "0")!
            self.btePrice.text = "$\(Double(round(100*bte)/100))"
            return
        }
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_GET_BTE_LAST, method: .get, parameter: nil, objectClass: BteFeedLastData.self, requestCode: U_GET_BTE_LAST, userToken: nil) { response in
            guard let value = response.bte?.replacingOccurrences(of: ",", with: "") else {return}
            let bte = Double(value ?? "0")!
            self.btePrice.text = "$\(Double(round(100*bte)/100))"
            Singleton.shared.bteFeedLastData = response
            DBManager.sharedInstance.addBTELastFeedData(id: response.id, aud: response.aud, usd: response.usd, bte: response.bte, no_shops: response.no_shops, users: response.users)
            self.businessCount.text = response.no_shops
            self.consumerCount.text = response.users
            ActivityIndicator.hide()
        }
    }
    
    func getCirculationCoin(){
        SessionManager.shared.methodForApiCalling(url:  U_GET_CIRCULATION_AMOUNT, method: .get, parameter: nil, objectClass: Response.self, requestCode: U_GET_CIRCULATION_AMOUNT, userToken: nil) { response in
            self.circulationLabel.text = "Circ: \(response.total_circulation ?? "")"
            UserDefaults.standard.setValue(response.total_circulation ?? "", forKey: UD_TOTAL_CIRCULATION)
        }
    }
}
