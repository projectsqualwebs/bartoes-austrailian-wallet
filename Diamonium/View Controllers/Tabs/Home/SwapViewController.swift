//
//  SwapViewController.swift
//  Diamonium
//
//  Created by Sagar Pandit on 19/10/21.
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit

class SwapViewController: UIViewController, CustomPickerDelegate, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(string == ""){
            self.initValue(val:String((textField.text ?? "")!.dropLast()))
        }else {
            self.initValue(val:(textField.text ?? "") + string)
        }
        
        return true
    }
    
    func getSelectedData(value: String) {
        self.pickerValue.text = value
        if(value == "BTE"){
            self.availableCurrencyLabel.text = Singleton.shared.unclearedAmount.available_bte
        }else {
            self.availableCurrencyLabel.text = Singleton.shared.unclearedAmount.available_bte_aud
        }
        self.initValue(val:"")
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var pickerValue: UILabel!
    @IBOutlet weak var availableCurrencyLabel: UILabel!
    @IBOutlet weak var swapValue: UITextField!
    @IBOutlet weak var swapLabel: UILabel!
    @IBOutlet weak var bteLabel: UILabel!
    @IBOutlet weak var swapButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initValue(val:"")
        self.swapValue.delegate = self
    }

    func initValue(val:String){
        let string = val  == "" ? "0":val
        if(self.pickerValue.text == "BTE"){
            self.availableCurrencyLabel.text = "AVAILABLE BTE: \(Singleton.shared.unclearedAmount.available_bte ?? "")"
            self.bteLabel.text = "BTE amount to swap"
            let amount = Double((Singleton.shared.unclearedAmount.available_bte ?? "0").replacingOccurrences(of: ",", with: ""))! - Double(string ?? "0")!
            self.swapLabel.text = "AVAILABLE BTE AFTER SWAP: \((amount*100).rounded()/100)"
            self.swapButton.setTitle("Swap to BTE-AUD", for: .normal)
        }else {
            self.availableCurrencyLabel.text = "AVAILABLE BTE-AUD: \(Singleton.shared.unclearedAmount.available_bte_aud ?? "")"
            self.bteLabel.text = "BTE-AUD amount to swap"
            let amount = Double((Singleton.shared.unclearedAmount.available_bte_aud ?? "0").replacingOccurrences(of: ",", with: ""))! - Double(string ?? "0")!
            self.swapLabel.text = "AVAILABLE BTE-AUD AFTER SWAP: \((amount*100).rounded()/100)"
            self.swapButton.setTitle("Swap to BTE", for: .normal)
        }
        
    }
    
    //MARK: IBActions
    @IBAction func pickerAction(_ sender: Any) {
        self.showAlert(title: "Alert", message: "Trade Dollars (BTE-AUD) exchange facility will be made available some time in the future after BTE lists on an open exchange. You will be notified when this occurs", action1Name: "Ok", action2Name: nil)
        return
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomPickerViewController") as! CustomPickerViewController
//        myVC.modalPresentationStyle = .overFullScreen
//        myVC.arrayCryptos = ["BTE","BTE-AUD"]
//        myVC.customPickerdelegate = self
//        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func swapAction(_ sender: Any){
        self.showAlert(title: "Alert", message: "Trade Dollars (BTE-AUD) exchange facility will be made available some time in the future after BTE lists on an open exchange. You will be notified when this occurs", action1Name: "Ok", action2Name: nil)
        return
//        ActivityIndicator.show(view: self.view)
//    let param:[String:Any] = [
//                "amount":self.swapValue.text,
//                "from":self.pickerValue.text,
//                "to":self.pickerValue.text == "BTE" ? "BTEAUD":"BTE"
//            ]
//
//        SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_TOKEN_SWAP, method: .post, parameter: param, objectClass: Response.self, requestCode: U_TOKEN_SWAP, userToken: nil) { Response in
//            ActivityIndicator.hide()
//            self.showAlert(title: Response.message ?? "", message: nil, action1Name: "Ok", action2Name: nil)
//            self.initValue(val: "")
//            NotificationCenter.default.post(name: NSNotification.Name("updateBalance"), object: nil)
//        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
