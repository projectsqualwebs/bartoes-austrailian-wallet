//
//  OrderDetailsViewController.swift
//  Diamonium
//
//

import UIKit

class OrderDetailsViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var imageQrCode: UIImageView!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var clipboard: View!
    @IBOutlet weak var labelOrderId: UILabel!
    @IBOutlet weak var labelPaymentType: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var labelDIAM: UILabel!
    
    var address: String?
    var amount: String?
    var bte: String?
    var paymentType: String?
    var orderId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelAddress.text = address ?? ""
        labelNote.text = "Please pay \(amount ?? "0") \(paymentType!) to above address. \(bte ?? "0") BTE will be added once payment confirmed."
        imageQrCode.image = generateQRCode(from: labelAddress.text ?? "")
        showPaymentDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNavigationBar(true)
    }
    
    func showPaymentDetails() {
        labelOrderId.text = orderId
        labelPaymentType.text = paymentType
        labelAmount.text = amount
        labelDIAM.text = bte
    }
    
    @objc override func back() {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }

    //MARK: Action
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: labelAddress.text)
    }
}
