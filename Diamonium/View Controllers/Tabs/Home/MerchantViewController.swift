//
//  MerchantViewController.swift
//  Diamonium
//
//  Created by Qualwebs on 26/09/23.
//  Copyright © 2023 Qualwebs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Lottie

class MerchantViewController: UIViewController, UpdateProfileDelegate {
    func updateProfile(name: String?, profileImage: UIImage?) {
        userImage.image = profileImage
        labelUsername.text = name
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var labelAccountType: UILabel!
    @IBOutlet weak var labelAccountNumber: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var rewardAmount: UILabel!
    @IBOutlet weak var voucherView: UIView!
    @IBOutlet weak var voucherField: UITextField!
    @IBOutlet weak var customerNumberField: UITextField!
    
    @IBOutlet weak var popupView: View!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var transferConfirmPopUp: View!
    @IBOutlet weak var labeConfirmFrom: UILabel!
    @IBOutlet weak var labelConfirmTo: UILabel!
    @IBOutlet weak var labelConfirmAmount: UILabel!
    
    @IBOutlet weak var animationView: AnimationView!
    
    
    var voucherInfoType = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserData), name: NSNotification.Name("updateUserData"), object: nil)
        
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.5
        animationView.play()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.voucherView.isHidden = true
        self.voucherField.text = ""
        self.customerNumberField.text = ""
        self.updateUserData()
        self.apiCallToGetDiamBalance()
    }
    
    func  apiCallToGetDiamBalance() {
        if(DBManager.sharedInstance.currentUser.count > 0){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_GET_CLEARED_AMOUNT + "\(DBManager.sharedInstance.currentUser[0].accNumber ?? "")", method: .get, parameter: nil, objectClass: GetUnclearedAmount.self, requestCode: U_GET_CLEARED_AMOUNT, userToken: nil) { response in
                let data = self.getValueRemovingComma(response: response)
                Singleton.shared.unclearedAmount = data
                DBManager.sharedInstance.addCirculationAmount(total_bte: data.total_bte, available_bte: data.available_bte, bte_aud: data.bte_aud, total_aud: data.total_aud, available_bte_aud: data.available_bte_aud, available_aud: data.available_aud, be_rewards: data.be_rewards)
                self.rewardAmount.text = "$\(data.be_rewards ?? "0")"
                
                ActivityIndicator.hide()
            }
        }
    }
    
    
    @objc func updateUserData() {
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerLast()
        PageViewController.index_delegate?.getControllerIndex(index: 5)
        if(DBManager.sharedInstance.currentUser.count > 0){
            labelUsername.text = DBManager.sharedInstance.currentUser[0].name ?? "Not Available"
            labelAccountType.text = DBManager.sharedInstance.currentUser[0].accType ?? "Personal"
            if(labelAccountType.text == "Individual"){
                self.labelAccountType.text = "Personal"
            }
            if(labelAccountType.text == "Individual"){
                self.labelAccountType.text = "Personal"
            }
            labelAccountNumber.text = DBManager.sharedInstance.currentUser[0].accNumber
            if let image = DBManager.sharedInstance.currentUser[0].profileImage, image != "" {
                userImage.sd_setImage(with: URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
            } else {
                userImage.image = #imageLiteral(resourceName: "defaultProfile.png")
            }
        }
    }
    
    
    func callRedeemApi(token:String){
        ActivityIndicator.show(view: self.view)
        let param = [
            "voucher_id": self.voucherField.text ?? "",
            "user_id": self.customerNumberField.text ?? ""
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_REDEEM_VOUCHER, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_PURCHASE_BTE_CRYPTOS, userToken: token) { response in
            self.loginView.isHidden = true
            self.labeConfirmFrom.text = DBManager.sharedInstance.currentUser[0].accNumber
            self.labelConfirmTo.text = self.customerNumberField.text ?? ""
            self.labelConfirmAmount.text = "$\(response.amount ?? 0)"
            self.transferConfirmPopUp.isHidden = false
            self.popupView.isHidden = false
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func showProfile(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        controller.updateProfileDelegate = self
        self.puchController(controller: controller)
    }
    
    @IBAction func redeemAction(_ sender: UIButton) {
        if(sender.tag == 1){
            self.voucherView.isHidden = false
        }else if(sender.tag == 2){
            self.voucherView.isHidden = true
            self.voucherField.text = ""
            self.customerNumberField.text = ""
            let pageVC = PageViewController.dataSource1 as? PageViewController
            pageVC?.setControllerThird()
            PageViewController.index_delegate?.getControllerIndex(index: 2)

        }else {
            self.voucherView.isHidden = true
            self.voucherField.text = ""
            self.customerNumberField.text = ""
        }
    }
    
    @IBAction func scanVoucherAction(_ sender: UIButton) {
        let scanContoller = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
        scanContoller.isRedirectionFromVoucher = true
        self.voucherInfoType = sender.tag
        ScanViewController.scanDelegate = self
        self.present(scanContoller, animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if(self.voucherField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter voucher code", action1Name: "Ok", action2Name: nil)
        }else if(self.customerNumberField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter customer code", action1Name: "Ok", action2Name: nil)
        }else {
            if let token = UserDefaults.standard.value(forKey: K_BTE_CRYPTO_TOKEN) as? String{
                self.callRedeemApi(token:token)
            }else if((DBManager.sharedInstance.currentUser[0].email ?? "").isEmpty){
                self.showAlert(title: "Error", message: "Please complete your profile first", action1Name: "Ok", action2Name: nil)
            }else {
                self.emailField.text = ""
                self.passwordField.text = ""
                self.emailField.text = DBManager.sharedInstance.currentUser[0].email ?? ""
                self.loginView.isHidden = false
                self.transferConfirmPopUp.isHidden = true
                self.popupView.isHidden = false
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.popupView.isHidden = true
    }
    
    @IBAction func generateTokenAction(_ sender: Any) {
        if(self.emailField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Email Address", action1Name: "Ok", action2Name: nil)
        }else if(self.passwordField.text!.isEmpty){
            self.showAlert(title: "Error", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GENERATE_TOKEN, method: .post, parameter: ["email":self.emailField.text, "password":self.passwordField.text], objectClass: RegisterUser.self, requestCode: U_GENERATE_TOKEN, userToken: nil) { response in
                ActivityIndicator.hide()
                self.popupView.isHidden = true
                if(response.token != "" || response.token != nil){
                    UserDefaults.standard.setValue(response.token ?? "", forKey: K_BTE_CRYPTO_TOKEN)
                    self.callRedeemApi(token:response.token ?? "")
                }else {
                    self.showAlert(title: "Error", message: response.message ?? "", action1Name: "Ok", action2Name: nil)
                }
                
            }
        }
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.popupView.isHidden = true
        self.transferConfirmPopUp.isHidden = true
        self.voucherView.isHidden = true
        self.customerNumberField.text = ""
        self.voucherField.text = ""
    }
    
}


extension MerchantViewController: ScanQRCode {
    func getUsernameByScan(val: String?) {
        if(self.voucherInfoType == 1){
            self.voucherField.text = val
        }else {
            self.customerNumberField.text = val
        }
    }
}
