//
//  BuyDiamViewController.swift
//  Diamonium
//
//

import UIKit
import SkyFloatingLabelTextField

class BuyDiamViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var buttonBalanceMode: UIButton!
    @IBOutlet weak var textFieldBTC: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldToken: UITextField!
    @IBOutlet weak var textFieldPrice: UITextField!
    @IBOutlet weak var textFieldBonus: UITextField!
    
    var value: Double?
    var valueType: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func resetFields() {
        self.buttonBalanceMode.setTitle("BTC", for: .normal)
        self.textFieldBTC.placeholder = "BTC"
        self.textFieldBTC.title = "BTC:"
        self.textFieldBTC.text = ""
        self.textFieldToken.text = ""
        self.textFieldPrice.text = ""
        self.textFieldBonus.text = ""
    }
    
    //MARK: Action
    @IBAction func selectBalanceMode(_ sender: UIButton) {
        let balanceActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        balanceActionSheet.addAction(UIAlertAction(title: "BTC", style: .default, handler: { _ in
            self.buttonBalanceMode.setTitle("BTC", for: .normal)
            self.textFieldBTC.placeholder = "BTC"
            self.textFieldBTC.title = "BTC:"
            self.textFieldBTC.text = ""
            self.textFieldToken.text = ""
            self.textFieldPrice.text = ""
            self.textFieldBonus.text = ""
        }))
        balanceActionSheet.addAction(UIAlertAction(title: "ETH", style: .default, handler: { _ in
            self.buttonBalanceMode.setTitle("ETH", for: .normal)
            self.textFieldBTC.placeholder = "ETH"
            self.textFieldBTC.title = "ETH:"
            self.textFieldBTC.text = ""
            self.textFieldToken.text = ""
            self.textFieldPrice.text = ""
            self.textFieldBonus.text = ""
        }))
        balanceActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        balanceActionSheet.view.tintColor = primaryColor
        self.present(balanceActionSheet, animated: true, completion: nil)
    }
    
    @IBAction func buyNow(_ sender: UIButton) {
        if (textFieldBTC.text?.isEmpty)! && (textFieldToken.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter \((buttonBalanceMode.titleLabel?.text)!) or BTE", action1Name: "Ok", action2Name: nil)
        } else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_GENERATE_ADDRESS, method: .post, parameter: [K_COIN_TYPE: (buttonBalanceMode.titleLabel?.text?.lowercased())!, K_AMOUNT: textFieldBTC.text!, K_TOKENS : textFieldToken.text!, K_BONUS_TOKEN: textFieldBonus.text!], objectClass: GenerateAddress.self, requestCode: U_BUY_TOKEN, userToken: nil) { response in
                let orderDetail = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
                orderDetail.address = response.address
                orderDetail.paymentType = self.buttonBalanceMode.titleLabel?.text
//                orderDetail.btcAmount = self.textFieldBTC.text
//                orderDetail.diam = self.textFieldToken.text
//                orderDetail.bonus = self.textFieldBonus.text
                self.resetFields()
                self.navigationController?.pushViewController(orderDetail, animated: true)
                ActivityIndicator.hide()
            }
        }
    }
}
extension BuyDiamViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            value = 0.0
        } else {
            value = Double(textField.text ?? "0")!
        }
        if textField == textFieldBTC {
            valueType = K_BTC_VALUE
        } else if textField == textFieldToken {
            valueType = K_DIAM_VALUE
        }
        if textField == textFieldToken && value! < 100.0 {
            showAlert(title: "Error", message: "BTE should not be less than 100", action1Name: "Ok", action2Name: nil)
        } else {
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE+U_BUY_TOKEN, method: .post, parameter: [K_COIN_TYPE: (buttonBalanceMode.titleLabel?.text?.lowercased())!, K_VAL_TYPE: valueType!, K_VAL : value!], objectClass: BuyToken.self, requestCode: U_BUY_TOKEN, userToken: nil) { response in
                self.textFieldToken.text = response.tokens?.description
                self.textFieldBTC.text = response.coin_amount?.description
                self.textFieldPrice.text = response.rate_per_token
                self.textFieldBonus.text = response.bonus?.description
                ActivityIndicator.hide()
            }
        }
    }
}
