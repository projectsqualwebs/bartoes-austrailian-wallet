//
//  HomeSliderViewController.swift
//  Diamonium
//
//

import UIKit

class HomeSliderViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDiam: UILabel!
    @IBOutlet weak var labelBonus: UILabel!
    
    var content: SliderContent?
    var itemIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = content?.date
        labelDiam.text = content?.diam
        labelBonus.text = content?.bonus
    }
    
    //MARK: Action
    @IBAction func previous(_ sender: UIButton) {
        let pageVC = SinglePageViewController.dataSource1  as! SinglePageViewController
        pageVC.goToPage(animated: true, completion: nil, next: false)
    }
    
    @IBAction func next(_ sender: UIButton) {
        let pageVC = SinglePageViewController.dataSource1  as! SinglePageViewController
        pageVC.goToPage(animated: true, completion: nil, next: true)
    }
}

struct SliderContent {
    var date: String?
    var diam: String?
    var bonus: String?
}
