//
//  SplashContentViewController.swift
//  Diamonium
//


import UIKit

class SplashContentViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelDynamicContent: UILabel!
    
    var content: String?
    var itemIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDynamicContent.text = content
    }
}
