//
//  EnterPasscodeViewController.swift
//  Diamonium
//
//  Created by AM on 18/09/19.
//

import UIKit
import KWVerificationCodeView

var passCodeScreen = ""

class EnterPasscodeViewController: UIViewController {
   

    //MARK: IBOutlets
    @IBOutlet weak var passcodeView: KWVerificationCodeView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var backView: UIView!
    
    
    var password = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        passcodeView.becomeFirstResponder()
        if let pass = UserDefaults.standard.value(forKey: K_USER_PASSCODE) as? String {
            self.password = pass
            continueButton.setTitle("Continue", for: .normal)
        }else {
            continueButton.setTitle("Submit", for: .normal)
        }
        
        if(passCodeScreen == "AmountScreen"){
           backView.isHidden = false
        }else if(passCodeScreen == "Appdelegate"){
            backView.isHidden = false
        }else {
            backView.isHidden = true
        }
    }
    
    //MARK: IBACtions
    @IBAction func submitPasscode(_ sender: Any) {
        let code = passcodeView.getVerificationCode()
        if code == " " {
            showAlert(title: "Error", message: "Enter Passcode", action1Name: "Ok", action2Name: nil)
        } else if (code.count != 6) {
            showAlert(title: "Incorrect Passcode", message: "Passcode must be 6 digits long", action1Name: "Ok", action2Name: nil)
        }else {
            if(continueButton.titleLabel?.text == "Submit"){
                UserDefaults.standard.setValue(code, forKey: K_USER_PASSCODE)
                if(passCodeScreen == "AmountScreen"){
                    NotificationCenter.default.post(name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
                    self.navigationController?.popViewController(animated: false)
                }else if(passCodeScreen == "PrivateKey"){
                    self.navigationController?.popViewController(animated: false)
                }else if(passCodeScreen == "Appdelegate"){
                    //self.dismiss(animated: false, completion: nil)
                    DispatchQueue.main.async {
                        Router.launchTabVC()
                        Router.apiCallToGetSocial()
                    }
                }else {
                    DispatchQueue.main.async {
                        Router.launchTabVC()
                        Router.apiCallToGetSocial()
                    }
                }
            }else {
                if(self.password != code){
                            showAlert(title: "Incorrect Passcode", message: "Please enter correct Passcode", action1Name: "Ok", action2Name: nil)
                }else if(passCodeScreen == "PrivateKey"){
                    self.navigationController?.popViewController(animated: false)
                }else {
                    UserDefaults.standard.setValue(code, forKey: K_USER_PASSCODE)
                    if(passCodeScreen == "AmountScreen"){
                        NotificationCenter.default.post(name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
                         self.navigationController?.popViewController(animated: false)
                    }else if(passCodeScreen == "Appdelegate"){
                        //self.dismiss(animated: false, completion: nil)
                        DispatchQueue.main.async {
                            Router.launchTabVC()
                            Router.apiCallToGetSocial()
                        }
                    }else if(passCodeScreen == "PrivateKey"){
                        self.navigationController?.popViewController(animated: false)
                    }else {
                      // self.navigationController?.popViewController(animated: false)
                        DispatchQueue.main.async {
                            Router.launchTabVC()
                            Router.apiCallToGetSocial()
                        }
                    }
                }
            }
           
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        if(passCodeScreen == "AmountScreen"){
         self.navigationController?.popViewController(animated: false)
        }else if(passCodeScreen == "Appdelegate"){
            //self.dismiss(animated: false, completion: nil)
            DispatchQueue.main.async {
                Router.launchSplash()
            }
        }else if(passCodeScreen == "PrivateKey"){
            passCodeScreen = ""
            self.navigationController?.popViewController(animated: false)
        }
        else {
            // self.navigationController?.popViewController(animated: false)
//            DispatchQueue.main.async {
//                Router.launchSplash()
//            }
    }
    }
    
}
