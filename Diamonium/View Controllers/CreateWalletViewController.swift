//
//  CreateWalletViewController.swift
//  Diamonium
//


import UIKit
import FlagPhoneNumber
import SkyFloatingLabelTextField

class CreateWalletViewController: UIViewController, NumberVerified{
    
    func registerUser(phoneNumber: String) {
        
                    var currencyName = self.getCountryPhonceCode(self.countryCode ?? "")
                    self.labelUsername.text = phoneNumber
                    self.labelUsernamePrefix.text = currencyName + "-" + (self.countryCode ?? "")
                    self.labelMobilePrefix.text = currencyName
                    self.textFieldMobile.text = phoneNumber
                    self.apiCallToRegister(number: phoneNumber,code:self.countryCode ?? "",iso: currencyName)
        
    }
    
    //MARK: IBOutlet
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var labelMobilePrefix: UILabel!
    //    @IBOutlet weak var labelCountryCode: UILabel!
    @IBOutlet weak var textFieldMobile: FPNTextField!
    @IBOutlet weak var textFieldFullName: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    @IBOutlet weak var labelUsernamePrefix: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var referralCode: SkyFloatingLabelTextField!
    @IBOutlet weak var referralCodeImage: UIImageView!
    @IBOutlet weak var privateKeyImage: ImageView!
    
    var isValidUser: Bool = false
    var countryCode: String?
    var businessType: String = "Personal"
    // var _accountKit: AccountKit!
    let RISTRICTED_CHARACTERS = "'*=+[]\\|;:'\",<>/?%"
    var savePrivateKey = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        if _accountKit == nil {
        //            _accountKit = AccountKit(responseType: .accessToken)
        //
        //        }
        textFieldMobile.font = UIFont.systemFont(ofSize: 14)
        textFieldMobile.delegate = self
        textFieldFullName.delegate = self
        referralCode.delegate = self
        labelMobilePrefix.text = Locale.current.currencyCode
        countryCode = textFieldMobile.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "")
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            textFieldMobile.setFlag(for: FPNCountryCode(rawValue: countryCode)!)
        }
        //  var local = Locale.current
        // local.regionCode = "IN"
    }
    
    func passwordVisiblity(ofTextField textField: UITextField) {
        if textField.isSecureTextEntry {
            textField.isSecureTextEntry = false
        } else {
            textField.isSecureTextEntry = true
        }
    }
    
    //MARK: Action
    @IBAction func back(_ sender: UIButton) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            businessType = "Personal"
            break
        case 1:
            businessType = "Business"
            break
        case 2:
            businessType = "Non-Profit"
            break
        default:
            break
        }
        if !((textFieldMobile.text?.isEmpty)!) {
            labelUsernamePrefix.text = ((labelMobilePrefix.text)?.lowercased())!+"-"+(countryCode ?? "")
            self.isValidUser = false
            self.apiCallToValidateUser((labelUsernamePrefix.text!)+(labelUsername.text!)) { (validate) in
                self.isValidUser = validate
            }
        }
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        if sender.tag == 0 {
            passwordVisiblity(ofTextField: textFieldPassword)
        } else {
            passwordVisiblity(ofTextField: textFieldConfirmPassword)
        }
    }
    
    @IBAction func create(_ sender: UIButton) {
        if (textFieldMobile.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Mobile Number", action1Name: "Ok", action2Name: nil)
        }else if(self.textFieldFullName.text!.count < 3){
            self.showAlert(title: "Error", message: "Full Name should be greater than 2 characters", action1Name: "Ok", action2Name: nil)
        }else if(self.textFieldFullName.text!.count > 20){
            self.showAlert(title: "Error", message: "Full Name should be less than 20 characters", action1Name: "Ok", action2Name: nil)
        }else if !isValidUser {
            showAlert(title: "Error", message: "Mobile number already exists in databse", action1Name: "Ok", action2Name: nil)
        }
        else if (textFieldPassword.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter password", action1Name: "Ok", action2Name: nil)
        } else if (textFieldPassword.text?.characters.count)! < 6 {
            showAlert(title: "Error", message: "Enter valid password", action1Name: "Ok", action2Name: nil)
        } else if textFieldPassword.text != textFieldConfirmPassword.text {
            showAlert(title: "Error", message: "Password doesn't match", action1Name: "Ok", action2Name: nil)
        } else {
            ActivityIndicator.show(view: self.view)
            let mobileRemoveSpace = self.textFieldMobile.text?.replacingOccurrences(of: " ", with: "")
            let mobileRemoveDash = mobileRemoveSpace?.replacingOccurrences(of: "-", with: "")

            SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_GENERATE_OTP, method: .post, parameter: ["mobile_number": (countryCode ?? "")+(mobileRemoveDash!)], objectClass: Response.self, requestCode: U_GENERATE_OTP, userToken: nil) { response in
                
                
                
                let otpController = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                otpController.phone = mobileRemoveDash ?? ""
                otpController.numberDelegate = self
                otpController.countryCode = self.countryCode ?? ""
                self.navigationController?.pushViewController(otpController, animated: true)
                ActivityIndicator.hide()
            }
        }
    }
    
    @IBAction func savePrivateKeyAction(_ sender: Any) {
        if(self.privateKeyImage.image == UIImage(named: "checkmark")){
            self.privateKeyImage.image = nil
            self.savePrivateKey = 0
        }else {
            self.privateKeyImage.image = UIImage(named: "checkmark")
            self.savePrivateKey = 1
        }
    }
    
}

extension CreateWalletViewController: UITextFieldDelegate {
    func verifyReferralCode(text:String){
        SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_VERIFY_REFFERAL_CODE + text, method: .get, parameter: nil, objectClass: Response.self, requestCode: U_VERIFY_REFFERAL_CODE, userToken: nil) { response in
            self.referralCodeImage.image = #imageLiteral(resourceName: "checkCircle")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == referralCode){
            self.referralCodeImage.image = nil
            var text = (textField.text ?? "") + string
            if(string == ""){
                text.removeLast()
            }
            if(text.count >= 16){
                self.verifyReferralCode(text: text)
            }
        }else if textField == textFieldMobile {
            if((textField.text?.hasPrefix("0"))!){
                //self.showAlert(title: "Error", message: "Mobile number cannot start with 0", action1Name: "Ok", action2Name: nil)
                textField.text = ""
                labelUsername.text = ""
            }
            if labelUsername.text == "" {
                labelUsernamePrefix.text = (labelMobilePrefix.text)!+"-"+(countryCode ?? "")
                labelUsername.text = string
            } else if string != "" {
                labelUsername.text! += string
            } else {
                labelUsername.text?.remove(at: (labelUsername.text?.index(before: (labelUsername.text?.endIndex)!))!)
            }
            self.isValidUser = false
            SessionManager.shared.createWallet = true
            
            if(textFieldMobile.text!.count >= 8 && textFieldMobile.text!.count < 12 ){
                self.apiCallToValidateUser((labelUsernamePrefix.text!)+(labelUsername.text!)) { (validate) in
                    self.isValidUser = validate
                }
            }
            return true
        }else if (textFieldFullName == textField) {
            if string.rangeOfCharacter(from: .letters) != nil || string == " " ||  string == "" {
                return true
            }else {
                return false
            }
        }
        return true
    }
}
extension CreateWalletViewController: FPNTextFieldDelegate {
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        countryCode = dialCode.replacingOccurrences(of: "+", with: "")
        let currency = (Locale.currency[code])!
        if let currencyCode = (currency as? (String, String))?.0 {
            labelMobilePrefix.text = currencyCode
        }
        isValidUser = false
        labelUsernamePrefix.text = (labelMobilePrefix.text)!+"-"+(countryCode ?? "")
        textFieldMobile.text = ""
        labelUsername.text = ""
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        //        if isValid {
        //            print(textField)
        //        } else {
        //            showAlert(title: "Error", message: "Enter Valid Mobile Number", action1Name: "Ok", action2Name: nil)
        //        }
    }
}

extension CreateWalletViewController {
    //otpController.fullName = self.textFieldFullName.text
    //                otpController.accType = self.businessType
    //                otpController.username = ((self.labelUsernamePrefix.text!)+(self.labelUsername.text!))
    //                otpController.password = self.textFieldPassword.text
    //                otpController.otp = response.otp
    
    func apiCallToRegister(number: String,code:String,iso:String) {
        ActivityIndicator.show(view: self.view)
        var userName =  (iso.lowercased())+"-"+(code)
        let param = [K_USERNAME:((userName)+(number)).lowercased(),
                     K_PASSWORD: self.textFieldPassword.text!,
                     K_FULL_NAME:  self.textFieldFullName.text!,
                     K_PROFILE_IMAGE: "",
                     K_ACC_TYPE: self.businessType,
                     K_FIREBASE_TOKEN: Singleton.shared.firebaseToken,
                     "referrer":self.referralCode.text,
                     "store_key":self.savePrivateKey,
                     K_DEVICE_TYPE: 1] as [String : Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_REGISTER, method: .post, parameter: param, objectClass: Register.self, requestCode: U_REGISTER, userToken: nil) { response in
            
            UserDefaults.standard.set(response.token, forKey: K_TOKEN)
            let mobileRemoveSpace = self.textFieldMobile.text?.replacingOccurrences(of: " ", with: "")
            let mobileRemoveDash = mobileRemoveSpace?.replacingOccurrences(of: "-", with: "")
            DBManager.sharedInstance.addUser(name: self.textFieldFullName.text!, email: nil, phone:(self.countryCode ?? "") + mobileRemoveDash!, token: response.token, key: response.key, accType: self.businessType, accNumber: ((self.labelUsernamePrefix.text!)+(self.labelUsername.text!)).lowercased(), profileImage: nil, bteBalance: "0", currencyBalance: "0")
            //            Router.getTransactionHistory(transId: "1.11.0")
            
            let homeController = self.storyboard?.instantiateViewController(withIdentifier: "TabsViewController") as! TabsViewController
            if let navController = self.navigationController {
                navController.pushViewController(homeController, animated: true)
            } else {
                self.view.window?.rootViewController?.dismiss(animated: true, completion: {
                    NotificationCenter.default.post(name: NSNotification.Name("updateUserData"), object: nil)
                })
            }
            ActivityIndicator.hide()
        }
    }
    
    
    //    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AccessToken!, state: String!) {
    //        self._accountKit = AccountKit(responseType: ResponseType.accessToken)
    //        self._accountKit.requestAccount {
    //            (account, error) -> Void in
    //            if let phoneNumber = account?.phoneNumber{
    //                var currencyName = self.getCountryPhonceCode(phoneNumber.countryCode)
    //
    //                self.labelUsername.text =  phoneNumber.phoneNumber
    //                self.labelUsernamePrefix.text = currencyName + "-" + phoneNumber.countryCode
    //                self.labelMobilePrefix.text = currencyName
    //                self.textFieldMobile.text = phoneNumber.phoneNumber
    //                self.apiCallToRegister(number: phoneNumber.phoneNumber,code:phoneNumber.countryCode,iso: currencyName)
    //
    //            }
    //        }
    //   }
    
    //    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
    //        // ... implement appropriate error handling ...
    //        print("\(viewController) did fail with error: \(error.localizedDescription)")
    //    }
    //
    //    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
    //        // ... handle user cancellation of the login process ...
    //    }
    
    func getCountryPhonceCode (_ country : String) -> String
    {
        var countryDictionary  = ["AF":"93",
                                  "AL":"355",
                                  "DZ":"213",
                                  "AS":"1",
                                  "AD":"376",
                                  "AO":"244",
                                  "AI":"1",
                                  "AG":"1",
                                  "AR":"54",
                                  "AM":"374",
                                  "AW":"297",
                                  "AU":"61",
                                  "AT":"43",
                                  "AZ":"994",
                                  "BS":"1",
                                  "BH":"973",
                                  "BD":"880",
                                  "BB":"1",
                                  "BY":"375",
                                  "BE":"32",
                                  "BZ":"501",
                                  "BJ":"229",
                                  "BM":"1",
                                  "BT":"975",
                                  "BA":"387",
                                  "BW":"267",
                                  "BR":"55",
                                  "IO":"246",
                                  "BG":"359",
                                  "BF":"226",
                                  "BI":"257",
                                  "KH":"855",
                                  "CM":"237",
                                  "CA":"1",
                                  "CV":"238",
                                  "KY":"345",
                                  "CF":"236",
                                  "TD":"235",
                                  "CL":"56",
                                  "CN":"86",
                                  "CX":"61",
                                  "CO":"57",
                                  "KM":"269",
                                  "CG":"242",
                                  "CK":"682",
                                  "CR":"506",
                                  "HR":"385",
                                  "CU":"53",
                                  "CY":"537",
                                  "CZ":"420",
                                  "DK":"45",
                                  "DJ":"253",
                                  "DM":"1",
                                  "DO":"1",
                                  "EC":"593",
                                  "EG":"20",
                                  "SV":"503",
                                  "GQ":"240",
                                  "ER":"291",
                                  "EE":"372",
                                  "ET":"251",
                                  "FO":"298",
                                  "FJ":"679",
                                  "FI":"358",
                                  "FR":"33",
                                  "GF":"594",
                                  "PF":"689",
                                  "GA":"241",
                                  "GM":"220",
                                  "GE":"995",
                                  "DE":"49",
                                  "GH":"233",
                                  "GI":"350",
                                  "GR":"30",
                                  "GL":"299",
                                  "GD":"1",
                                  "GP":"590",
                                  "GU":"1",
                                  "GT":"502",
                                  "GN":"224",
                                  "GW":"245",
                                  "GY":"595",
                                  "HT":"509",
                                  "HN":"504",
                                  "HU":"36",
                                  "IS":"354",
                                  "IN":"91",
                                  "ID":"62",
                                  "IQ":"964",
                                  "IE":"353",
                                  "IL":"972",
                                  "IT":"39",
                                  "JM":"1",
                                  "JP":"81",
                                  "JO":"962",
                                  "KZ":"77",
                                  "KE":"254",
                                  "KI":"686",
                                  "KW":"965",
                                  "KG":"996",
                                  "LV":"371",
                                  "LB":"961",
                                  "LS":"266",
                                  "LR":"231",
                                  "LI":"423",
                                  "LT":"370",
                                  "LU":"352",
                                  "MG":"261",
                                  "MW":"265",
                                  "MY":"60",
                                  "MV":"960",
                                  "ML":"223",
                                  "MT":"356",
                                  "MH":"692",
                                  "MQ":"596",
                                  "MR":"222",
                                  "MU":"230",
                                  "YT":"262",
                                  "MX":"52",
                                  "MC":"377",
                                  "MN":"976",
                                  "ME":"382",
                                  "MS":"1",
                                  "MA":"212",
                                  "MM":"95",
                                  "NA":"264",
                                  "NR":"674",
                                  "NP":"977",
                                  "NL":"31",
                                  "AN":"599",
                                  "NC":"687",
                                  "NZ":"64",
                                  "NI":"505",
                                  "NE":"227",
                                  "NG":"234",
                                  "NU":"683",
                                  "NF":"672",
                                  "MP":"1",
                                  "NO":"47",
                                  "OM":"968",
                                  "PK":"92",
                                  "PW":"680",
                                  "PA":"507",
                                  "PG":"675",
                                  "PY":"595",
                                  "PE":"51",
                                  "PH":"63",
                                  "PL":"48",
                                  "PT":"351",
                                  "PR":"1",
                                  "QA":"974",
                                  "RO":"40",
                                  "RW":"250",
                                  "WS":"685",
                                  "SM":"378",
                                  "SA":"966",
                                  "SN":"221",
                                  "RS":"381",
                                  "SC":"248",
                                  "SL":"232",
                                  "SG":"65",
                                  "SK":"421",
                                  "SI":"386",
                                  "SB":"677",
                                  "ZA":"27",
                                  "GS":"500",
                                  "ES":"34",
                                  "LK":"94",
                                  "SD":"249",
                                  "SR":"597",
                                  "SZ":"268",
                                  "SE":"46",
                                  "CH":"41",
                                  "TJ":"992",
                                  "TH":"66",
                                  "TG":"228",
                                  "TK":"690",
                                  "TO":"676",
                                  "TT":"1",
                                  "TN":"216",
                                  "TR":"90",
                                  "TM":"993",
                                  "TC":"1",
                                  "TV":"688",
                                  "UG":"256",
                                  "UA":"380",
                                  "AE":"971",
                                  "GB":"44",
                                  "US":"1",
                                  "UY":"598",
                                  "UZ":"998",
                                  "VU":"678",
                                  "WF":"681",
                                  "YE":"967",
                                  "ZM":"260",
                                  "ZW":"263",
                                  "BO":"591",
                                  "BN":"673",
                                  "CC":"61",
                                  "CD":"243",
                                  "CI":"225",
                                  "FK":"500",
                                  "GG":"44",
                                  "VA":"379",
                                  "HK":"852",
                                  "IR":"98",
                                  "IM":"44",
                                  "JE":"44",
                                  "KP":"850",
                                  "KR":"82",
                                  "LA":"856",
                                  "LY":"218",
                                  "MO":"853",
                                  "MK":"389",
                                  "FM":"691",
                                  "MD":"373",
                                  "MZ":"258",
                                  "PS":"970",
                                  "PN":"872",
                                  "RE":"262",
                                  "RU":"7",
                                  "BL":"590",
                                  "SH":"290",
                                  "KN":"1",
                                  "LC":"1",
                                  "MF":"590",
                                  "PM":"508",
                                  "VC":"1",
                                  "ST":"239",
                                  "SO":"252",
                                  "SJ":"47",
                                  "SY":"963",
                                  "TW":"886",
                                  "TZ":"255",
                                  "TL":"670",
                                  "VE":"58",
                                  "VN":"84",
                                  "VG":"284",
                                  "VI":"340"]
        var keys = countryDictionary.allKeysForValue(val:country)
        var currentCountry = [String]()
        if(keys.count > 1){
            currentCountry = keys.filter{$0 == Locale.current.regionCode!}
        }else {
            currentCountry = keys
        }
        if(currentCountry != []){
            var myCurrencyCode = Locale.myCurrency[currentCountry[0]]
            textFieldMobile.setFlag(for: FPNCountryCode(rawValue: currentCountry[0])!)
            print(myCurrencyCode?.code)
            return myCurrencyCode!.code!
        }else {
            return ""
        }
    }
    
}

extension Dictionary where Value : Equatable {
    func allKeysForValue(val : Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}


extension Locale {
    static let myCurrency: [String: (code: String?, symbol: String?)] = Locale.isoRegionCodes.reduce(into: [:]) {
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: $1]))
        $0[$1] = (locale.currencyCode, locale.currencySymbol)
    }
}
