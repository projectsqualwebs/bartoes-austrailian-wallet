//
//  APIManager.swift
//  Diamonium

import UIKit
import Alamofire

class SessionManager: NSObject {
    
    static var shared = SessionManager()
    
    var createWallet: Bool = true
    
    
    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        
        Alamofire.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers: getHeader(reqCode: requestCode, userToken: userToken)).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")
            switch dataResponse.result {
            case .success(_):
                let object = self.convertDataToObject(response: dataResponse.data, T.self)
                let errorObject = self.convertDataToObject(response: dataResponse.data, Response.self)
                
                if (statusCode == 200 || statusCode == 201) && object != nil
//                    && (requestCode != U_VALIDATE_USER)
                {
                    completionHandler(object!)
                } else if statusCode == 404 && self.createWallet {
//                    if (UserDefaults.standard.string(forKey: K_TOKEN) == nil) &&
                        if requestCode == U_VALIDATE_USER {
                        completionHandler(object!)
                    } else {
                        //Showing error message on alert
                        //                        UIApplication.shared.keyWindow?.rootViewController?.showAlert(message: message!, title: "Error", isPopBack: false)
                    }
                    //                    self.webServiceDelegate?.dataNotFound!(msg: message)
                } else {
                    if !self.createWallet {
                        self.showAlert(msg: "Username not available")
                    } else {
                        if((errorObject?.message ?? "") != ""){
                            self.showAlert(msg: errorObject?.message)
                        }
                    }
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
                ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                self.showAlert(msg:error)
                } else {
                    //Showing error message on alert
                    if((error ?? "") != ""){
                        self.showAlert(msg: dataResponse.error!.localizedDescription ?? "")
                    }else {
                        self.showAlert(msg: "Blockchain is on maintenance, please check app in sometime, if anything urgent please contact support")
                    }
                }
                break
            }
        }
    }
    
    
    
    
    func apiCallToGetBalance(token: String? = nil, completionHandler: @escaping (String, String, String) -> Void) {
        if token == nil {
            ActivityIndicator.show(view: (UIApplication.shared.keyWindow?.rootViewController?.view)!)
        }
        
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_GET_BALANCE+Singleton.shared.selectedCurrency.code, method: .get, parameter: nil, objectClass: Balances.self, requestCode: U_GET_BALANCE, userToken: token) { response in
            
            let balance = (response.local_value ?? "").lowercased() == "nan" ? "0":(response.local_value ?? "")
            let tokenBalance = (response.be_value ?? "").lowercased() == "nan" ? "0":(response.be_value ?? "")
            let beRewards = (response.be_rewards ?? "").lowercased() == "nan" ? "0":(response.be_rewards ?? "")

            if token == nil {
                UserDefaults.standard.set(balance, forKey: K_BALANCE)
                UserDefaults.standard.set(tokenBalance, forKey: K_TOKEN_BALANCE)
            }
            completionHandler(balance, tokenBalance, beRewards)
        }
    }
    
    private func showAlert(msg: String?) {
        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: nil, message: msg, action1Name: "Ok", action2Name: nil)

    }
    
    func makeMultipartRequest(url: String, fileData: Data, param: String, fileName: String, completionHandler: @escaping (String) -> Void) {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append (fileData, withName: param , fileName: fileName, mimeType: "image/jpg")
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: getHeader(reqCode: U_UPLOAD_WALLET_IMAGE, userToken: nil)) { (encodingResult) in
            
            switch encodingResult {
            case .success(let response,_,_):
                response.responseString(completionHandler: { (dataResponse) in
                    
                    ActivityIndicator.hide()
                    
                    let errorObject = self.convertDataToObject(response: dataResponse.data, Response.self)
                    
                    if dataResponse.response?.statusCode == 200 {
                        let object = self.convertDataToObject(response: dataResponse.data, ImagePath.self)
                        completionHandler((object?.path)!)
                    } else {
                        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: errorObject?.message, action1Name: "Ok", action2Name: nil)
                    }
                })
                break
            case .failure(let error):
                //Showing error message on alert
                UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: error.localizedDescription, action1Name: "Ok", action2Name: nil)
                break
            }
        }
    }
    
     func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }
    
    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        let token = UserDefaults.standard.string(forKey: K_TOKEN)
        if (reqCode != U_LOGIN) && (reqCode != U_REGISTER) && (reqCode != U_GENERATE_OTP) && (reqCode != U_VALIDATE_USER) && (reqCode != U_GET_TOKEN) && (token != nil) && (reqCode != U_UPDATE_PASSWORD) && (reqCode != U_VERIFY_KEY) && (reqCode != U_SUPPORT_FORM ){
            if userToken == nil {
                return ["Authorization": "Bearer "+token!]
            } else {
                return ["Authorization": "Bearer "+userToken!]
            }
        } else {
            return nil
        }
    }
}
