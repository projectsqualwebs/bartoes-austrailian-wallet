//
//  ImagesPageViewController.swift
//  Ezhal
//


import UIKit
protocol PageContentIndexDelegate {
    func getContentIndex(index: Int)
}

class SinglePageViewController: UIPageViewController {

    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: PageContentIndexDelegate? = nil
    static var controller: String = K_SPLASH
    var currentIndex: Int = 0
    var arrayContent: [Any]?
//    var arrayHomeSliderContent: [SliderContent] = [SliderContent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if SinglePageViewController.controller != K_SPLASH {
//            removeSwipeGesture()
            arrayContent = [SliderContent]()
            arrayContent?.append(SliderContent(date: "15 Jul 2021", diam: "12 BTE", bonus: "11"))
            arrayContent?.append(SliderContent(date: "17 Jul 2021", diam: "14 BTE", bonus: "15"))
        } else {
            arrayContent = ["Backed to Goods & Services", "Barter Trade Currency", "Super Fast and Scalable", "Privacy for Everyone"]
            self.dataSource = self
            self.delegate = self
        }
        
        getContent()
        SinglePageViewController.dataSource1 = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SinglePageViewController.dataSource1 = self
    }
    
    func getContent() {
        self.setViewControllers([contentAtIndex(index: currentIndex)!], direction: .forward, animated: false, completion: nil)
    }
    
    func contentAtIndex(index: Int) -> UIViewController? {
        if (arrayContent?.count == 0) || (index >= (arrayContent?.count)!) {
            return nil
        }
        if SinglePageViewController.controller == K_SPLASH {
            let splashContent = self.storyboard?.instantiateViewController(withIdentifier: "SplashContentViewController") as! SplashContentViewController
            guard let content = arrayContent as? [String] else { return nil }
            splashContent.content = content[index]
            splashContent.itemIndex = index
            return splashContent
        } else {
            let homeSliderContent = self.storyboard?.instantiateViewController(withIdentifier: "HomeSliderViewController") as! HomeSliderViewController
            guard let content = arrayContent as? [SliderContent] else { return nil }
            homeSliderContent.content = content[index]
            homeSliderContent.itemIndex = index
            return homeSliderContent
        }
    }
    
    func viewAtIndex(_ viewController: UIViewController, next: Bool) -> UIViewController? {
        var index = 0
        if SinglePageViewController.controller == K_SPLASH {
            let pageContent: SplashContentViewController = viewController as! SplashContentViewController
            index = pageContent.itemIndex
        } else {
            let pageContent: HomeSliderViewController = viewController as! HomeSliderViewController
            index = pageContent.itemIndex
        }
        
        if next {
            if (index == NSNotFound) {
                return nil
            }
            index += 1
            if index == arrayContent?.count {
                return nil
            }
        } else {
            if ((index == 0) || (index == NSNotFound)) {
                return nil
            }
            index -= 1
        }
        return contentAtIndex(index: index)
    }
}
extension SinglePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: false)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: true)
    }
}
extension SinglePageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if SinglePageViewController.controller == K_SPLASH {
            let currentVC: SplashContentViewController = pageViewController.viewControllers?[0] as! SplashContentViewController
            currentIndex = Int(currentVC.itemIndex)
        } else {
            let currentVC: HomeSliderViewController = pageViewController.viewControllers?[0] as! HomeSliderViewController
            currentIndex = Int(currentVC.itemIndex)
        }
        SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
    }
}

extension UIPageViewController {
    func goToPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil, next: Bool) {
        if let currentViewController = viewControllers?[0] {
            if next {
                if let nextPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerAfter: currentViewController) {
                    setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
                }
            } else {
                if let previousPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerBefore: currentViewController) {
                    setViewControllers([previousPage], direction: .reverse, animated: animated, completion: completion)
                }
            }
        }
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}
