//
//  Extemsions.swift
//  Diamonium


import UIKit
import AVFoundation

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIViewController {
    func getValueRemovingComma(response:GetUnclearedAmount) -> GetUnclearedAmount {
        let total_bte = response.total_bte?.replacingOccurrences(of: ",", with: "")
        let available_bte = response.available_bte?.replacingOccurrences(of: ",", with: "")
        let bte_aud = response.bte_aud?.replacingOccurrences(of: ",", with: "")
        let total_aud = response.total_aud?.replacingOccurrences(of: ",", with: "")
        let available_bte_aud = response.available_bte_aud?.replacingOccurrences(of: ",", with: "")
        let available_aud = response.available_aud?.replacingOccurrences(of: ",", with: "")
        return GetUnclearedAmount(total_bte: total_bte, available_bte: available_bte, bte_aud: bte_aud, total_aud: total_aud, available_bte_aud: available_bte_aud, available_aud: available_aud)
    }
    
    
    func appendString(data: Double) -> String { // changed input type of data
        let value = data
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2 // for float
        formatter.maximumFractionDigits = 2 // for float
        formatter.minimumIntegerDigits = 1
        formatter.paddingPosition = .afterPrefix
        formatter.paddingCharacter = "0"
        return formatter.string(from: NSNumber(floatLiteral: value))! // here double() is not required as data is already double
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func setNavigationBar(_ back: Bool) {
        self.navigationItem.title = "BARTEOS GLOBAL WALLET"
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = barBackgroundColor
        navigationController?.navigationBar.tintColor = UIColor.black
        if back {
            var leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(self.back))
            navigationItem.leftBarButtonItem = leftBarButtonItem
        }
    }
    
    @objc func back() {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func leftBarButtonAction() {
        
    }
    
    func puchController(controller: UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func presentController(controller: UIViewController) {
        DispatchQueue.main.async {
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func popToTabController() {
        for controller in (navigationController?.viewControllers)! {
            if controller is TabsViewController {
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        var myVal = Int()
        var intValue:Int64 = 10000000000
        if(timestamp/Int(truncatingIfNeeded: intValue) == 0){
            myVal = timestamp
        }else {
            myVal = timestamp/1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(myVal))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    
    func apiCallToValidateUser(_ username: String, completionHanlder: @escaping (Bool) -> Void) {
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_VALIDATE_USER, method: .post, parameter: [K_USERNAME: username], objectClass: Response.self, requestCode: U_VALIDATE_USER, userToken: nil) { response in
            if(response.message! == "Username already taken") {
                completionHanlder(false)
            } else {
                completionHanlder(true)
            }
            ActivityIndicator.hide()
        }
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 11, y: 11)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                //                guard let qrImage = output.tinted(using: primaryColor) else { return nil }
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func hideClipboard(_ view: UIView) {
        view.isHidden = true
    }
    
    func copyText(object: UIView, text: String?) {
        object.isHidden = false
        UIPasteboard.general.string = text
        self.perform(#selector(self.hideClipboard(_:)), with: object, afterDelay: 1)
    }
}

extension CIImage {
    /// Inverts the colors and creates a transparent image by converting the mask to alpha.
    /// Input image should be black and white.
    var transparent: CIImage? {
        return inverted?.blackTransparent
    }
    
    /// Inverts the colors.
    var inverted: CIImage? {
        guard let invertedColorFilter = CIFilter(name: "CIColorInvert") else { return nil }
        
        invertedColorFilter.setValue(self, forKey: "inputImage")
        return invertedColorFilter.outputImage
    }
    
    /// Converts all black to transparent.
    var blackTransparent: CIImage? {
        guard let blackTransparentFilter = CIFilter(name: "CIMaskToAlpha") else { return nil }
        blackTransparentFilter.setValue(self, forKey: "inputImage")
        return blackTransparentFilter.outputImage
    }
    
    /// Applies the given color as a tint color.
    func tinted(using color: UIColor) -> CIImage?
    {
        guard
            let transparentQRImage = transparent,
            let filter = CIFilter(name: "CIMultiplyCompositing"),
            let colorFilter = CIFilter(name: "CIConstantColorGenerator") else { return nil }
        
        let ciColor = CIColor(color: color)
        colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter.outputImage
        
        filter.setValue(colorImage, forKey: kCIInputImageKey)
        filter.setValue(transparentQRImage, forKey: kCIInputBackgroundImageKey)
        
        return filter.outputImage!
    }
}

extension Locale {
    static let currency = Locale.isoRegionCodes.reduce(into: [:]) {
        let locale = Locale(identifier: Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: $1]))
        $0[$1] = (locale.currencyCode, locale.currencySymbol)
    }
}

extension UILabel {
    var isTruncated: Bool {
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}

extension String {
    func matches(_ regex: String) -> Bool {
        return NSPredicate(format:"SELF MATCHES %@", regex).evaluate(with: self)
    }
}

extension StringProtocol {
    var double: Double? {
        return Double(self)
    }
   
}
