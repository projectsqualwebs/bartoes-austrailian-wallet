//
//  CustomPickerViewController.swift
//  Diamonium
//


protocol CustomPickerDelegate {
    func getSelectedData(value: String)
}

import UIKit

class CustomPickerViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var customPicker: UIPickerView!
    
    var customPickerdelegate: CustomPickerDelegate? = nil
    var arrayCryptos = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(arrayCryptos.count == 0){
          apiCallToGetSupportedCryptos()
        }
    }
    
    func apiCallToGetSupportedCryptos() {
        ActivityIndicator.show(view: self.view)
        
        SessionManager.shared.methodForApiCalling(url: U_BASE+U_SUPPORTED_CRYPTOS, method: .get, parameter: nil, objectClass: SupportedCryptos.self, requestCode: U_SUPPORTED_CRYPTOS, userToken: nil) { (response) in
            
            if let cryptos = response.cryptos, cryptos.count > 0 {
                self.arrayCryptos = cryptos
                self.customPicker.reloadAllComponents()
            }
            ActivityIndicator.hide()
        }
    }
    
    //MARK: Action
    @IBAction func dismiss(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func done(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                let selectedRow = self.customPicker.selectedRow(inComponent: 0)
                self.customPickerdelegate?.getSelectedData(value: self.arrayCryptos[selectedRow])
            })
        }
    }
}
extension CustomPickerViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayCryptos.count
    }
}
extension CustomPickerViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayCryptos[row].uppercased()
    }
}
