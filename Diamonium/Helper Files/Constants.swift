//
//  APIConstants.swift
//  Diamonium
//


import UIKit

//MARK: Constants
//var primaryColor = UIColor(red: 167/255, green: 206/255, blue: 69/255, alpha: 1)
var primaryColor = UIColor(red: 8/255, green: 103/255, blue: 54/255, alpha: 1)// 086736
//    UIColor(red: 229/255, green: 178/255, blue: 69/255, alpha: 1)
var secondaryColor = UIColor(red: 219/255, green: 183/255, blue: 55/255, alpha: 1)
var barBackgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
var backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
var offWhiteColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
var goldenColor = UIColor(red: 198/255, green: 166/255, blue: 51/255, alpha: 1)//C6A633
var lightGreen = UIColor(red: 205/255, green: 252/255, blue: 175/255, alpha: 1)//CDFCAF
//lighgreen CDFCAF
//var U_BASE2 = "http://149.28.171.247:7000/"

//MARK: API Constants
//Base Url
var U_BASE = "https://barteos.exchange/"

//"https://propertycoins.org/"
//918109502280
//var U_BASE2 = "http://104.156.233.218:7000/"
var U_BASE3 = "http://104.156.233.218/"

var U_CREATE_SUPPORT_TICKET = "https://besupport.qualwebs.com/api/tickets.json";

//var U_BASE2 = "http://149.28.171.247:7000/"
var U_BASE2 = "http://45.32.200.14:7000/"
//Url end-points
var U_GET_BTE_FEEDS = "https://barteos.exchange/get-bte-feeds"
var U_GET_BTE_LAST = "https://barteos.exchange/get-bte-last"
var U_WEBVIEW =  "https://support.barteos.org/create-ticket"
var U_REGISTER = "register"
var U_LOGIN = "login"
var U_GET_BALANCE = "get-balance?currency="
var U_TRANSFER = "transfer"
var U_VALIDATE_USER = "validate-user"
var U_BUY_TOKEN = "buy-token"
//"https://propertycoins.org/buy-token"
var U_GENERATE_ADDRESS = "generate-address"
var U_GET_USERNAMES = "get-usernames-new"
var U_GET_TRANSACTIONS = "get-transactions?trxid="
var U_GET_SINGLE_TRANSACTION = "get-single-transaction?trxid="
var U_UPLOAD_WALLET_IMAGE = "wallet-image-upload"
var U_PROFILE_UPDATE = "profile-update"
var U_CURRENCY_CONVERT = "currency_convert"
var U_ADD_CONTACT = "add-contact"
var U_GET_CONTACTS = "get-contacts?username="
var U_GET_FEEDS = "api/feed"
var U_SOCIAL = "api/social"
var U_CALCULATE_BTE = "api/calculate-bte"
var U_USER_EXISTS = "api/user-exists"
var U_GET_TOKEN = "api/get-token"
var U_REGISTER_USER = "api/register-user"
var U_PURCHASE_BTE = "api/purchase-bte"
var U_SUPPORTED_CRYPTOS = "api/supported-cryptos"
var U_VERIFY_KEY = "verify-key"
var U_UPDATE_PASSWORD = "update-password"
var U_DELETE_CONTACT = "delete-contact?contact_id="
var U_SUPPORT_FORM = "https://support.barteos.org/api/ticket/create"
var U_GENERATE_OTP = "generate-otp"
var U_VERIFY_OTP = "verify-otp"
var U_GET_CLEARED_AMOUNT = "https://barteos.exchange/get-cleared-amount?walletId="

var U_VERIFY_REFFERAL_CODE = "validate-referral?referrer="
var U_GET_REWARD_VALUE = "api/get-app-global-settings"
var U_UPDATE_KYC = "update-kyc"
var U_TOKEN_SWAP = "token-swap"


var U_GET_SUPPORTED_CRYPTOS = "api/supported-cryptos"
var U_CALCULATE_BTE_FROM_CRYPTO = "api/calculate-bte"
var U_PURCHASE_BTE_CRYPTOS = "api/purchase-bte"
var U_GET_CIRCULATION_AMOUNT = "https://barteos.exchange/api/get-circulation"
var U_GENERATE_TOKEN = "api/get-token"
var U_GET_TOPUP_ORDER = "api/get-orders"

var U_VALIDATE_REWARD = "api/reward/validate"
var U_TRANSFER_MONEY = "transfer-v2"
var U_GET_REWARD = "api/reward/transactions?wallet_address="

var U_UPDATE_PAYPAL_ORDER = "api/update-paypal-order"
var U_GET_USER_VOUCHER = "api/user/voucher/"
var U_REDEEM_VOUCHER = "api/user/voucher/redeem"
var U_GET_PACKAGES = "api/purchase/packages"


//API keys
var K_STATUS = "status"
var K_MESSAGE = "message"
var K_RESPONSE = "response"
var K_PHONE = "phone"
var K_OTP = "otp"
var K_NAME = "name"
var K_USERNAME = "username"
var K_EMAIL = "email"
var K_PASSWORD = "password"
var K_FULL_NAME = "full_name"
var K_PROFILE_IMAGE = "profile_image"
var K_ACC_TYPE = "acc_type"
var K_FIREBASE_TOKEN = "firebase_token"
var K_DEVICE_TYPE = "device_type"
var K_KEY = "key"
var K_TOKEN = "token"
var K_BALANCES = "balances"
var K_DIAM = "BTE"
var K_DPAY = "DPAY"
var K_TO = "to"
var K_ASSET = "asset"
var K_AMOUNT = "amount"
var K_MEMO = "memo"
var K_FROM = "from"
var K_BLOCK_NUM = "block_num"
var K_COIN_TYPE = "coin_type"
var K_VAL_TYPE = "val_type"
var K_VAL = "val"
var K_COIN_AMOUNT = "coin_amount"
var K_TOKENS = "tokens"
var K_BONUS = "bonus"
var K_TOTAL_TOKENS = "total_tokens"
var K_BONUS_RATE = "bonus_rate"
var K_RATE_PER_TOKEN = "rate_per_token"
var K_BONUS_TOKEN = "bonus_token"
var K_ADDRESS = "address"
var K_KEYWORDS = "keywords"
var K_CURRENCY_FROM = "currency_from"
var K_CURRENCY_TO = "currency_to"
var K_CONTACT_NAME = "contact_name"
var K_PREFERRED_PAYMENT = "preferred_payment"
var K_AMOUNT_TYPE = "amount_type"
var K_PRIVATE_KEY = "private_key"
var K_REFER_AMOUNT = ""

//Userdefault keys
var K_BUSINESS_TYPE = "business_type"
var K_AUTH = "biometric_auth"
var K_TIMER = "timer"
var K_AUTOLOCK_NAME = "auto_lock_name"
var K_RESIGN_APP_TIMESTAMP = "resign_app_timestamp"
var K_CURRENCY_DETAIL = "currency_detail"
var K_TOKEN_BALANCE = "token_balance"
var K_BALANCE = "balance"
var K_USER_PASSCODE = "user_passcode"
var K_BTE_CRYPTO_TOKEN = "K_BTE_CRYPTO_TOKEN"

//Static keys
var K_BTC_VALUE = 0
var K_DIAM_VALUE = 1

var K_SENT = 1
var K_RECEIVE = 2

let K_TRANSFER_FEES: Double = 0.01//20

//Controller id's
var K_SPLASH = "splash"
var K_HOME_SLIDER = "home_slider"

//Regex
var QRCODE_REGEX = "[a-zA-Z0-9-]+[+0-9.]+[+0-9.]+[a-zA-Z0-9-]+[+0-9.]+[a-zA-Z0-9-]+"
var QRCODE_REGEX2 = "[a-zA-Z0-9-]+[+0-9.]+[+0-9.]+[a-zA-Z0-9-]+[+0-9.]+ "
var QRCODE_REGEX3 = "[a-zA-Z0-9-]+[+0-9.]+[+0-9.]+[a-zA-Z0-9-]+[+0-9.]+"

//Notification
var N_ENTER_PASSCODE = "enter_passcode"
var N_PRESENT_PASSCODE = "present_passcode_screen"

var UD_TOTAL_CIRCULATION = "UD_TOTAL_CIRCULATION"
var UD_BTE_FEED = "UD_BTE_FEED"
