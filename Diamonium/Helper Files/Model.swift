//
//  Model.swift
//  Diamonium
//


import UIKit

struct Response: Codable {
    var message: String?
//    var status: Int?
    var response: String?
    var total_circulation: String?
    var refer_amount: Int?
}


struct UserExist: Codable {
    var status: Bool?
}

struct RegisterUser: Codable {
    var status: String?
    var token: String?
    var message: String?
}

struct CreateWallet: Codable {
    var status: Int?
    var otp: Int?
}

struct Register: Codable {
    var message: String?
    var key: String?
    var token: String?
}

struct LoginResponse: Codable {
    var message: String?
    var key: String?
    var token: String?
    var data: [Login]?
}

struct Login: Codable {
    var name: String?
    var full_name: String?
    var email: String?
    var profile_image: String?
    var acc_type: String?
}


struct UploadImageResponse: Codable {
    let status: Bool
    let response: ImagePath?
}

struct ImagePath: Codable {
    let path: String?
}

struct ResetResponse: Codable {
    let status: Int?
}

struct Balances: Codable {
    var local_value: String?
    var be_value: String?
    var be_rewards: String?
}

struct CurrencyBalances: Codable {
    var local_value: Double?
    var be_value: Double?
}

struct FundTransfer: Codable {
    var trxid: String?
    var block_id: Int?
    var datetime: Int?
    var name: String?
    var to_name: String?
    var amount: String?
    var authorizer: String?
    var type: Int?
    var asset: String?
}

struct BuyToken: Codable {
    var coin_amount: Double?
    var tokens: Double?
    var bonus: Double?
    var total_tokens: Double?
    var bonus_rate: String?
    var rate_per_token: String?
}

struct GenerateAddress: Codable {
    var address: String?
}

struct SettingsHeading {
    var title: String?
    var content: [Settings]?
}

struct Settings {
    var icon: UIImage?
    var name: String?
    var url: String?
    var autoLock: AutoLock?
    var isSelected: Bool = false
    
    init(icon: UIImage, name: String, url: String? = nil) {
        self.icon = icon
        self.name = name
        self.url = url
    }
    
    init(icon: UIImage, autoLock: AutoLock) {
        self.icon = icon
        self.autoLock = autoLock
    }
}

struct Currency: Codable {
    let description: String
    let code: String
    let symbol: String
    var isSelected: Bool = false
}

struct FeedsResponse: Codable {
    var feeds: [NewsFeeds]?
}
//
//struct DisclaimerResponse: Codable {
//    let status: Bool
//    let response: Disclaimer?
//    let error: ErrorResponse?
//}
//
//struct Disclaimer: Codable {
//    let id: Int?
//    let disclaimer: String?
//    let disclaimer_ar: String?
//}

//struct OrderResponse: Codable {
//    var msg: String?
//    var response: Order
//    var status: String?
//}
//
//struct Order: Codable {
//    var orders: [Orders ]
//}

//struct Orders: Codable {
//    var id: Int?
//    var user_id: Int?
//    var order_id: String?
//    var coin_type: String?
//    var coin_value: String?
//    var bte_coin:Double?
//    var bonus_coin: Int?
//    var total_coin: Double?
//    var coin_live_price: Double?
//    var bte_baseprice: Double?
//    var bonus: Any?
//    var coin_address: String?
//    var precision: Int?
//    var status: String?
//    var currency_type: String?
//    var comments: Any?
//    var created_at: String?
//    var updated_at: String?
//}


struct NewsFeeds: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var url: String?
    var created_at: String?
    var created_timestamp: Int?
}

struct UsersData: Codable {
    var data: [Users]?
}

struct Users: Codable {
    var id: Int?
    var name: String?
    var contact_name: String?
    var full_name: String?
    var profile_image: String?
    
    init(id:Int?,name: String?, image: String?,full_name: String?) {
        self.id = id
        self.name = name
        self.full_name = full_name
        self.contact_name = name
        self.profile_image = image
    }
}

struct TransactionData: Codable {
    var data: [Transaction]?
}




struct Transaction: Codable {
    var trxid: String?
    var block_id: Int?
    var datetime: Int?
    var name: String?
    var to_name: String?
    var amount: String?
    var authorizer: String?
    var type: Int?
    var memoText: String?
    var asset: String?
    
    
    init(id: String?, name: String?, dateTime: Int?, amount: String, type: Int, blockId: Int,memoText: String?,to_name:String?,authorizer:String?,asset: String?) {
        self.trxid = id
        self.name = name
        self.amount = amount
        self.type = type
        self.datetime = dateTime
        self.block_id = blockId
        self.memoText = memoText
        self.to_name = to_name
        self.authorizer = authorizer
        self.asset = asset
    }
}

struct TransactionDetailData: Codable {
    var data: TransactionDetail?
}

struct TransactionDetail: Codable {
    var time: String?
    var authorizer: String?
    var block_id: Int?
}

struct SocialsData: Codable {
    var socials: [Socials]?
}

struct Socials: Codable {
    var id: Int?
    var name: String?
    var link: String?
}

struct CalculateBTE: Codable {
    var preferred_payment: String?
    var amount_type: String?
    var bte_amount: Double?
    var preferred_payable_amount: Double?
    var amount: Double?
}

struct PurchaseBTE: Codable {
    var message: String?
    var payment_address: String?
    var order_id: String?
}

struct SupportedCryptos: Codable {
    var cryptos: [String]?
}

//
struct Feeds {
    var newsFeeds: NewsFeeds?
    var readMore: Bool = false
}

struct GetUnclearedAmount: Codable {
    var total_bte: String?
    var available_bte: String?
    var bte_aud: String?
    var total_aud: String?
    var available_bte_aud: String?
    var available_aud: String?
    var be_rewards: String?
}

struct BTEFeeds: Codable {
    var bteFeed = [[Double]]()
    var user_feed = [[Double]]()
    var shop = [[Double]]()
}

struct BteFeedLastData: Codable {
    var id: Int?
    var aud:String?
    var usd: String?
    var bte: String?
    var no_shops: String?
    var users: String?
    
}

struct CoupenResponse: Codable {
    var message: String?
    var data = CoupenData()
}

struct CoupenData: Codable {
    var discount_amount: Double?
}

struct SendMoney: Codable{
    var cashbackData = CashbackData()
    var trxData = FundTransfer()
}

struct CashbackData: Codable {
    
}

//struct GetRewards: Codable {
//  var response
//}

struct GetPackages: Codable {
    var response = [PackageResponse]()
    var message: String?
    var status: Int?
}

struct PackageResponse: Codable {
    var id: Int?
    var title: String?
    var amount: String?

}

struct RewardResponse: Codable {
    var id: Int?
    var applied_user: String?
    var merchant: String?
    var coupen_code: String?
    var blockchain_trx_id: String?
    var trx_amount: String?
    var cashback_amount: String?
    var is_processed: Int?
    var created_at: String?
}

struct GetSupportedCrypto: Codable {
    var cryptos = [String]()
}

struct CalculateBteFromCrypto: Codable {
    var preferred_payment: String?
    var amount_type: String?
    var amount: String?
    var preferred_payable_amount: Double?
    var bte_amount: Double?
}

struct BteFromCryptoResponse: Codable {
    var message : String?
    var payment_address: String?
    var order_id: String?
    var bank_info: BankInfo?
}

struct BankInfo: Codable {
    var bank_name: String?
    var bank_account_number: String?
    var bank_code: String?
    
}

struct GetCryptoOrder: Codable {
    var orders : [CryptoOrderResponse]?
}

struct CryptoOrderResponse: Codable {
    var id: Int?
    var user_id: Int?
    var order_id: String?
    var coin_type: String?
    var coin_value: String?
    var bte_coin: Double?
    var bonus_coin: Double?
    var total_coin: Double?
    var coin_live_price: Double?
    var created_at: String?
    var status: String?
    var coin_address: String?
    
}

struct SuccessResponse: Codable {
    var amount: Double?
    var message: String?
    var status: String?
}
