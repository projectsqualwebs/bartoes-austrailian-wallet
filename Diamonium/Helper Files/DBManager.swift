//
//  DBManager.swift
//  Diamonium


import UIKit
import RealmSwift

class DBManager {
    
    static let sharedInstance = DBManager()
    
    private var database: Realm
    
    private init() {
        database = try! Realm()
    }
    
    var currentUser: Results<DBUsers> {
        do{
            return try (database.objects(DBUsers.self).filter("token == '\(Singleton.shared.userToken)'"))
        }catch{
            print("error")
        }
    }
    
    func getUserFromToken(_ token: String) -> DBUsers {
        do{
            if((database.objects(DBUsers.self).filter("token == '\(token)'")).count > 0){
                return (database.objects(DBUsers.self).filter("token == '\(token)'"))[0]
            }else {
                return DBUsers()
            }
        }catch{
            print("error")
        }
    }
    
    func getUserFromUsername(_ accNumber: String?) -> DBUsers {
        do{
            //        guard let accountNo = accNumber else {
            //            return (database.objects(DBUsers.self).filter("accNumber == '\(accNumber!.characters.suffix(10))'"))[0]
            //        }
            let user = (database.objects(DBUsers.self).filter("accNumber == '\(accNumber!)'"))
            if(user.count > 0){
                return user[0]
            }else {
                return DBUsers()
            }
        }catch{
            print("error")
        }
    }
    
    func userAvailability(accNumber: String) -> Bool {
        do{
            let user = (database.objects(DBUsers.self).filter("accNumber == '\(accNumber)'"))
            if user.count > 0 {
                return true
            } else {
                return false
            }
        }catch{
            print("error")
        }
    }
    
    func deleteTransactions() {
        do{
            try! database.write {
                let transaction = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'")
                database.delete(transaction)
            }
        }catch{
            print("error")
        }
    }
    
    func deleteContact(name:String) {
        do{
            try! database.write {
                let deleteConatact = database.objects(DBContacts.self).filter("name == '\(name)'")
                database.delete(deleteConatact)
            }
        }catch{
            print("error")
        }
    }
    
    func userContactAvailability(name: String) -> Bool {
        do{
            if(currentUser.count > 0){
                let user = currentUser[0].contacts.filter("name == '\(name)'")
                if user.count > 0 {
                    return true
                } else {
                    return false
                }
            }else {
                return false
            }
        }catch{
            print("error")
        }
    }
    
    //Users DB
    func getUsersFromDB() -> Results<DBUsers> {
        do{
            let results = database.objects(DBUsers.self)
            return results
        }catch{
            print("error")
        }
    }
    
    func addUser(name: String?, email: String?, phone: String?, token: String?, key: String?, accType: String?, accNumber: String?, profileImage: String?, bteBalance: String?, currencyBalance: String?) {
        do {
            let user = DBUsers()
            user.name = name
            user.email = email
            user.phone = phone
            user.token = token
            user.key = key
            user.accType = accType
            user.accNumber = accNumber
            user.profileImage = profileImage
            user.bteBalance = bteBalance
            user.currencyBalance = currencyBalance
            
            try? database.write {
                database.add(user)
            }
        }catch{
            print("error")
        }
    }
    
    func addCirculationAmount(total_bte: String?, available_bte: String?, bte_aud: String?, total_aud: String?, available_bte_aud: String?, available_aud: String?, be_rewards: String?){
        do{
            let data = CirculationAmount()
            data.total_bte =  total_bte
            data.available_bte = available_bte
            data.bte_aud = bte_aud
            data.total_aud = total_aud
            data.available_bte_aud = available_bte_aud
            data.available_aud = available_aud
            data.be_rewards = be_rewards
            
            try? database.write {
                database.add(data)
            }
        }catch{
            print("error")
        }
    }
    
    func getCircluationFromDB() -> CirculationAmount {
        do{
            guard let data = (database.objects(CirculationAmount.self)).first else {
                return CirculationAmount()
            }
            return data
        }catch{
            print("error")
        }
    }
    
    func addBTELastFeedData(id: Int?, aud: String?, usd: String?, bte: String?, no_shops: String?, users: String?){
        do{
            let data = BTEFeedLastData()
            data.id = id ?? 0
            data.aud = aud
            data.usd = usd
            data.bte = bte
            data.no_shops = no_shops
            data.users = users
            
            try? database.write {
                database.add(data)
            }
        }catch{
            print("error")
        }
    }
    
    func getBTELastFeedFromDB() -> BTEFeedLastData {
        do{
            guard let data = (database.objects(BTEFeedLastData.self)).first  else {
                return BTEFeedLastData()
            }
            return data
        }catch{
            print("error")
        }
    }
    
    func addUserContacts(contacts: [Users]) {
        do{
            for contact in contacts {
                let dbContact = DBContacts()
                dbContact.id = "\(contact.id!)"
                dbContact.name =  (contact.contact_name != nil) ? contact.contact_name:contact.name
                dbContact.contact_name = contact.name ?? ""
                dbContact.full_name = contact.full_name ?? ""
                dbContact.profileImage = contact.profile_image
                try! database.write {
                    currentUser[0].contacts.append(dbContact)
                }
            }
        }catch{
            print("error")
        }
    }
    
    func getContactsFromDB() -> List<DBContacts> {
        do{
            var results = List<DBContacts>()
            if(currentUser.count > 0){
                results =  currentUser[0].contacts
                return results
            }else {
                return results
            }
        }catch{
            print("error")
        }
    }
    
    func updateUserData(name: String?, email: String?, imagePath: String?, currency: Currency?, bteBalance: String?, currencyBalance: String?, accNumber: String? = nil, beRewards: String?) {
        do {
            let user = ((accNumber == nil) ? (DBManager.sharedInstance.currentUser[0]) : (self.getUserFromUsername(accNumber!)))
            
            if let username = name {
                try? database.write {
                    user.name = username
                    user.email = email
                    user.profileImage = imagePath ?? ""
                }
            }
            
            if let bteBal = bteBalance {
                try? database.write {
                    user.bteBalance = bteBal
                    user.currencyBalance = currencyBalance
                }
            }
            
            if let bteBal = beRewards {
                try? database.write {
                    user.beRewards = bteBal 
                }
            }
            
            if let currencyBal = currencyBalance {
                try? database.write {
                    user.currencyBalance = currencyBal
                }
            }
            
            if let currencyDetail = currency {
                try? database.write {
                    user.currencyDescription = currencyDetail.description
                    user.currencyCode = currencyDetail.code
                    user.currencySymbol = currencyDetail.symbol
                }
            }
        }catch{
            print("error")
        }
    }
    
    //Transaction DB
    func getTransactionFromDB() -> Results<DBTransaction> {
        do {
            let results = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'")
            return results
        }catch{
            print("error")
        }
    }
    
    func getLastTransactionId() -> String? {
        do {
            if(DBManager.sharedInstance.currentUser.count > 0){
                let results = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'").sorted(byKeyPath: "dateTime", ascending: false)
                if(results.count > 0){
                    return results[0].trxid
                }else {
                    return ""
                }
            }else {
                return ""
            }
        }catch{
        return ""
        print("error")
    }
}

func addTransaction(trxList: [Transaction], accNumber: String?) {
    do{
        var trxCount: Int = 0
        if trxList.count == 11 {
            trxCount = 10
        } else {
            trxCount = trxList.count
        }
        
        if(currentUser.count > 0){
            for index in 0..<trxCount {
                if (trxList[index].type != 3 && currentUser.count != 0) {
                    let transaction = DBTransaction()
                    transaction.name = trxList[index].name
                    transaction.trxid = trxList[index].trxid
                    transaction.amount = trxList[index].amount?.description
                    transaction.type = trxList[index].type?.description
                    transaction.profileImage = ""
                    transaction.blockId = trxList[index].block_id?.description
                    transaction.dateTime = trxList[index].datetime ?? 0
                    transaction.username = accNumber ?? currentUser[0].accNumber
                    transaction.memoText =  trxList[index].memoText ?? ""
                    transaction.authorizer = trxList[index].authorizer ?? ""
                    transaction.toName = trxList[index].to_name ?? ""
                    transaction.asset = trxList[index].asset ?? ""
                    try? database.write {
                        database.add(transaction)
                    }
                }
            }
        }
    }catch{
        print("error")
    }
}

//Delete DB
func deleteAllFromDB() {
    do{
        try? database.write {
            database.deleteAll()
        }
    }catch{
        print("error")
    }
}

//Social DB
func getSocialFromDB() -> Results<DBSocial> {
    do{
        let results = database.objects(DBSocial.self)
        return results
    }catch{
        print("error")
    }
}

func addSocial(name: String?, imageName: String?, url: String?) {
    do {
        let social = DBSocial()
        social.name = name
        social.imageName = imageName
        social.url = url
        
        try? database.write {
            database.add(social, update: .all)
        }
    }catch{
        print("error")
    }
}

//News feeds DB
func getClosedFeedsFromDB() -> Results<DBNewsFeed> {
    do{
        let results = database.objects(DBNewsFeed.self)
        return results
    }catch{
        print("error")
    }
}

func addClosedFeeds(newsFeed: NewsFeeds?) {
    do {
        let feeds = DBNewsFeed()
        feeds.id = newsFeed?.id?.description
        feeds.title = newsFeed?.title
        feeds.timestamp = newsFeed?.created_timestamp?.description
        feeds.feedDescription = newsFeed?.description
        feeds.url = newsFeed?.url
        
        try? database.write {
            database.add(feeds)
        }
    }catch{
        print("error")
    }
}
}
