//
//  File.swift
//  Diamonium
//

import UIKit

class ActivityIndicator
{
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    static func show(view: UIView) {
        DispatchQueue.main.async {
            self.overlayView = view
            self.overlayView.isUserInteractionEnabled = false
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.color = primaryColor
            view.addSubview(activityIndicator)
        }
    }
    
    static func hide() {
        overlayView.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
}

class SecondActivityIndicator
{
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    static func show(view: UIView) {
        DispatchQueue.main.async {
            self.overlayView = view
            self.overlayView.isUserInteractionEnabled = false
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.color = primaryColor
            view.addSubview(activityIndicator)
        }
    }
    
    static func hide() {
        overlayView.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
}
