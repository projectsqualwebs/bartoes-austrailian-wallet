//
//  Protocols.swift
//  Diamonium
//

import Foundation
import UIKit

protocol UsernameDelegate {
    func getUsernameFromList(selectedUser: Users, isContact: Bool)
}

protocol UpdateProfileDelegate {
    func updateProfile(name: String?, profileImage: UIImage?)
}
